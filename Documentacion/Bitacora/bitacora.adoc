# Segimiento de las semanas:

## Mes de Diciembre y Enero:

El mes de diciembre fue principalmente empezar el proyecto, pensar diseños, paleta de colores, como ibamos a estructurar el proyecto y que lenguajes ibamos a utilizar. No avanzamos mucho ya que nos llevamos gran parte de los meses decidiendo que ibamos a hacer y nos pillo las vacaciones, logramos terminar las vistas de la landin, login y registro.

## Mes de Febrero:

El mes de febrero, se nos añadio al grupo un nuevo integrante, Adrian Ramirez, decidimos hacer su proyecto ya que estaba mas avanzado ( nos llevamos 2 semanas con ese proyecto), pero al tenerlo que pasar todo a Laravel de nuevo, decidimos volver al proyecto que ya teniamos de los animales ya que es mas completo

- Empezamos a crear el Git
- Empezamos a crear el proyecto en laravel y pasar las vistas ya hechas. 
- Implementamos ademas la logica del Login y la logica del Registro (nueva vista) ademas de hacerlos responsive

## Mes de Marzo:

En el mes de marzo, fue un mes que avanzamos bastante ya que podiamos añadir animales y los motrabamos en el home para poder seleccionarlos y mostrabamos sus detalles simples.

- Se puede iniciar sesion con el nombre de usuario y con el correo, empezar el formulario de añadir animal y terminar
- Empezamos a crear la tabla de Pets
- Empezar a añadir animales 
- Relaciones de usuarios con sus animales 
- Creacion de los seeders de los animales y enlazarlos con usuarios
- Mostrar los animales con de cada usuario en el home
- Añadir animales a traves del formulario de añadir animales
- Creado el footer y header como partial y ponerlo en todas las paginas
- En el formulario de aladir animal podemos subir una foto del animal y se guarda
- Mostramos los detalles simples del animal

## Mes de Abril:

Este mes hemos empezamos a hacer ya lo que ofrecemos (comida, vacunas y actividades) y a implementarlo.

- Creado las vistas de los detalles de los animales y hacer la estructura
- Empezar a hacer la logica de los 3 apartados, cada uno con una

## Mes de Mayo:

Este mes ha sido principalmente de integrar la logica a la vista y hacer refactor general de la vista, hemos preparado el vagrant para que la pagina se levante a traves de un dominio.

- Implementar la logica a la vista y ajustarla a la ventana.
- Hacer refactor de toda la vista y que todo sea responsive.
- Comprobar errores varios.
- Configuracion del Vagrant.