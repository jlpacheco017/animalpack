<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Pets;
use App\Models\Razas;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddPetController extends Controller
{
    public function showAddPet()
    {
        $pet = Pets::all();
        $razas = Razas::all();
        return view('addPet.addPet', ['pet' => $pet, 'razas' => $razas]);
    }

    public function doAddPet(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'breed' => 'required',
            'fotoMascota' => 'image|max:2048',
            'weight' => 'required',
            'age' => [
                'required',
                'date',
                'before_or_equal:today',
            ],
        ],
        [
            'name.required' => 'El nombre es obligatorio.',
            'breed.required' => 'La raza es obligatoria.',
            'weight.required' => 'El peso es obligatorio.',
            'age.required' => 'El campo fecha es obligatorio.',
            'age.date' => 'El campo fecha debe ser una fecha válida.',
            'age.before_or_equal' => 'La fecha no puede ser superior a hoy.',
            'fotoMascota.image' => 'La imagen debe ser una imagen',
            'fotoMascota.max' => 'La imagen tiene que tener maximo 2MB'
        ]
    );
    $age = Carbon::createFromFormat('Y-m-d', $request->age);
    $diffYears = $age->diffInYears(Carbon::now());
    if ($diffYears > 20) {
        return back()->withErrors(['age' => 'La edad no puede ser mayor a 20 años.']);
    }
        $data = $request->all();
        $new_pet = new Pets;

            $new_pet->name =  $data['name'];
            $new_pet->age = $data['age'];
            $new_pet->breed = $data['breed'];
            $new_pet->weight = $data['weight'];
            $new_pet->user_id = Auth::user()->id;
            $new_pet->imagen = 'provisional';
            $new_pet->save();
        $lastDogInserted = Pets::find($new_pet->id);

        $imageDogName = Auth::user()->id . "-" . $lastDogInserted->id . ".jpg";

        $lastDogInserted->imagen = $imageDogName;
        $lastDogInserted->save();

        if ($request->hasFile('fotoMascota')) {
            $image = $request->file('fotoMascota');
            $image->move(public_path('img/pets'), $imageDogName);
        }
        return redirect(route("home.home"));
    }
}
