<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class authController extends Controller
{
    public function showLogin()
    {
        if (auth()->check()) {
            return redirect('/home');
        } else {
            return view('auth.login');
        }
    }

    public function logOut()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function doLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $login = $request->input('email');
        $password = $request->input('password');
        $remember = $request->input('remember');

        $user = Auth::attempt([
            'email' => $login,
            'password' => $password,
        ], $remember) || Auth::attempt([
            'username' => $login,
            'password' => $password,
        ], $remember);

        if ($user) {
                $user = User::where('email', $request->email)->first();
                if (isset($user)) {
                    session()->put('userLogeado', $user->username);
                    session()->put('userLogeadoId', $user->id);
                }
                else{
                    $user = User::where('username', $request->email)->first();
                    session()->put('userLogeado', $user->username);
                    session()->put('userLogeadoId', $user->id);
                }

            $request->session()->regenerate();
            
            return redirect()->intended(route('home.home'));
        } else {
            return back()->withErrors(['login' => 'El nombre de usuario/correo electrónico o contraseña son incorrectos.'])->withInput();
        }
    }



    public function showRegister()
    {
        return view('auth.register');
    }
    public function doRegister(Request $request)
    {
        $request->validate(
            [
                'username' => 'required|unique:users',
                'email' => 'required|email|unique:users',
                'name' => 'required',
                'password' => [
                    'required',
                    'min:8',
                    'regex:/^(?=.*[a-z])(?=.*[A-Z]).+$/'
                ]
            ],
            [
                'username.required' => 'El nombre de usuario es obligatorio',
                'username.unique' => 'El nombre de usuario ya existe',
                'email.required' => 'El correo electrónico es obligatorio',
                'email.email' => 'El correo electrónico no es válido',
                'email.unique' => 'El correo electrónico ya existe',
                'name.required' => 'El nombre completo es obligatorio',
                'password.required' => 'La contraseña es obligatoria',
                'password.min' => 'La contraseña no es válida',
                'password.regex' => 'La contraseña no es válida'
            ]
        );
        $data = $request->all();
        // $check = $this->create($data);
        User::create([
            'username' => $data['username'],
            'nombre' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'subscripcion' => 'Free'

        ]);

        // session()->flash()

        // $check = $this->create($data);

        return redirect()->route('auth.showLogin')->with('succes', 'Usuario creado con éxito.');
    }
}
