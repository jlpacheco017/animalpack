<?php

namespace App\Http\Controllers;

use App\Models\Pets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class homeController extends Controller
{
    public function index() {
        return view('home.index');
    }
    public function home() {
        $pet = Pets::getPetsFromUserId(Auth::user()->id);
        return view('home.home')->with("pet", $pet);
    }
}
