<?php

namespace App\Http\Controllers;

use App\Models\Pets;
use App\Models\Razas;
use Nette\Utils\DateTime;
use Illuminate\Http\Request;
use App\Models\PhysicalActivity;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class petDetailsController extends Controller
{
    public function showPetDetail($id) {
        if (Pets::findOrFail($id)->user_id == auth()->user()->id) {
            $animal = Pets::findOrFail($id);
            $razaInfo = Razas::where('raza', $animal->breed)->first();
            $numLimite = 0;

            if ($razaInfo->pmin >= 15) {
                $numLimite = 5;
            }
            if ($razaInfo->pmin < 15 && $razaInfo->pmin >= 10) {
                $numLimite = 3;
            }
            if ($razaInfo->pmin < 10 && $razaInfo->pmin >= 5) {
                $numLimite = 2;
            }
            if ($razaInfo->pmin <= 5) {
                $numLimite = 0.5;
            }
            $physicalActivity = PhysicalActivity::physycalActvityFromPet($id);

            $time = new DateTime("00:00");

            foreach ($physicalActivity as $pa) {
                $time->add(date_create($pa->started_at)->diff(date_create($pa->finished_at)));
            }

            $recomendedTime = PhysicalActivity::recomendedPhysicalActivity($id);
            $razas = Razas::all();

            return view('petDetails.petDetails', ['pet' => $animal, 'raza' => $razaInfo, 'numOperacion' => $numLimite, 'physicalActivity' => $physicalActivity, 'time' => $time->format('H:i:s'), "recomendedTime" => $recomendedTime, 'razas'=>$razas]);
        } else {
            return redirect()->route('home.home')->withErrors(['error' => 'No tienes acceso al animal seleccionado o no esta disponible su información']);
        }
    }
    public function editPet(Request $request) {
        $user = Auth::user();
        $pet = Pets::where('user_id', $user->id)->where('id', $request->id_pet)->first();
        if ($pet) {
            $request->validate([
                'name' => 'required',
                'razas' => 'required',
                'peso' => 'required',
                'year' => [
                    'required',
                    'date',
                    'before_or_equal:today',
                ],
            ],
            [   
                'name.required' => 'El nombre es obligatorio.',
                'razas.required' => 'La raza es obligatoria.',
                'peso.required' => 'El peso es obligatorio.',
                'year.required' => 'El campo fecha es obligatorio.',
                'year.date' => 'El campo fecha debe ser una fecha válida.',
                'year.before_or_equal' => 'La fecha no puede ser superior a hoy.',
            ]
        );
        $age = Carbon::createFromFormat('Y-m-d', $request->year);
        $diffYears = $age->diffInYears(Carbon::now());
        if ($diffYears > 20) {
            return back()->withErrors(['year' => 'La edad no puede ser mayor a 20 años.']);
        }
            $pet->name = $request->name;
            $pet->age = $request->year;
            $pet->breed = $request->razas;
            $pet->weight = $request->peso;
            $pet->save();
        }
       
    }
}
