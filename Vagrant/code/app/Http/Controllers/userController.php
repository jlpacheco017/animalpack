<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class userController extends Controller
{
    public function showUserDetail()
    {
        $usuario = User::findOrFail(session()->get('userLogeadoId'));
        return view('user.userDetail', ['user' => $usuario]);
    }
    public function editUserDetail(Request $request)
    {
        $userByUsername = User::where('username', $request->username)->first();

        $userByEmail = User::where('email', $request->email)->first();

        if ($userByUsername) {
            if ($userByUsername->username != auth()->user()->username) {
                if ($userByEmail->email == auth()->user()->email) {
                    return redirect()->route('home.home')->with('validation', 'El usuario ya existe.');
                }
            }
        }
        if ($userByEmail) {
            if ($userByEmail->email != auth()->user()->email) {
                if ($userByUsername->username == auth()->user()->email) {
                    return redirect()->route('home.home')->with('validation', 'El email ya existe.');
                }
            }
        }

        $user = User::find(auth()->user()->id);
        $user->username = $request->username;
        $user->nombre = $request->nombre;
        $user->email = $request->email;
        $user->save();

        session()->put('userLogeado', $request->username);
        return redirect()->route('home.home')->with('succes', 'Usuario actualizado con éxito.');
    }
}
