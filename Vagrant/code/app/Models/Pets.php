<?php

namespace App\Models;

use Ramsey\Uuid\Type\Integer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pets extends Model
{
    use HasFactory;
    protected $fillable = ['name','age','breed','weight','user_id'];


    static function getPetsFromUserId($id){
        return Pets::all()->where("user_id", $id);
    }
    
    public function user(){
        return $this->belongsTo(Pets::class);
    }
}
