<?php

namespace App\Models;

use App\Models\Pets;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PhysicalActivity extends Model
{
    use HasFactory;
    protected $fillable =
    [
        'started_at',
        'finished_at',
        'pet_id'
    ];

    public static function physycalActvityFromPet($id){
        return PhysicalActivity::all()->where("pet_id", $id);
    }

    public static function recomendedPhysicalActivity($id){
        $pet = Pets::all()->where("id", $id)->first();
        $breed = Razas::all()->where("raza", $pet->breed)->first();
        return $breed->recomendedTime;
    }
}
