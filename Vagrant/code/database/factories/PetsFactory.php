<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Pets>
 */
class PetsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $razas = [
            "Labrador Retriever",
            "Pastor Alemán",
            "Bulldog Inglés",
            "Golden Retriever",
            "Beagle",
            "Poodle",
            "Yorkshire Terrier",
            "Boxer",
            "Chihuahua",
            "Dachshund",
            "Rottweiler",
            "Siberian Husky",
            "Doberman Pinscher",
            "Shih Tzu",
            "Pomeranian",
            "Bichon Frise",
            "Great Dane",
            "Border Collie",
            "Cavalier King Charles Spaniel",
            "Boston Terrier",
            "Bernese Mountain Dog",
            "Australian Shepherd",
            "Shetland Sheepdog",
            "Shar Pei",
            "Jack Russell Terrier",
            "Cocker Spaniel",
            "English Springer Spaniel",
            "West Highland White Terrier",
            "Scottish Terrier",
            "Greyhound",
        ];
        $user = User::all();
        $raza = $razas[array_rand($razas, 1)];
        return [
            'name' => fake()->firstName(),
            'age' => fake()->date(),
            'imagen' => $raza . '.jpg',
            'breed' => $raza,
            'weight' => fake()->randomFloat(2, 5, 40),
            'user_id' =>  $user->random()->id,
        ];
    }
}
