<?php

namespace Database\Factories;

use App\Models\Pets;
use App\Models\PhysicalActivity;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PhysicalActivity>
 */
class PhysicalActivityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $fecha = date_create();
        $fecha2 = clone $fecha;
        date_modify($fecha2, '+' . random_int(0, 60) . ' minute');
        $pet = Pets::all();
        $startDate = $fecha;
        $endDate = $fecha2;

        return [
            'started_at' => $startDate,
            'finished_at' => $endDate,
            'pet_id' => $pet->random()->id
        ];
    }
}
