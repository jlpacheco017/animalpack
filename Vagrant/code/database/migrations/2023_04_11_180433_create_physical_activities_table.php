<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('physical_activities', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->timestamp("started_at")->nullable();
            $table->timestamp("finished_at")->nullable();
            $table->unsignedBigInteger('pet_id');

            $table->foreign('pet_id')->references('id')->on('pets');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('physical_activities');
    }
};
