<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PhysicalActivity;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PhysicalActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PhysicalActivity::factory(20)->create();
    }
}
