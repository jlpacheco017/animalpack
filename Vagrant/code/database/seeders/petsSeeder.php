<?php

namespace Database\Seeders;

use App\Models\Pets;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class petsSeeder extends Seeder
{
    public function run(): void
    {
        $animales = $this->command->ask('Cuantos animales quieres insertar? ',50);
        Pets::factory($animales)->create();
    }
}
