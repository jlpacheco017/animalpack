const vistaAvanzadaSelector = document.querySelector("#advanced-info-selector");
const vistaSimpleSelector = document.querySelector("#simple-info-selector");

const vistaAvanzadaContainer = document.querySelector("#advanced-info-container");
const vistaSimpleContainer = document.querySelector("#simple-info-container");


const vistaVacunasSelector = document.querySelector("#vista-vacunas-selector")
const vistaFormaFisicaSelector = document.querySelector("#vista-forma-fisisca-selector");
const vistaAlimentacionSelector = document.querySelector("#vista-alimentacion-selector");

const vistaVacunasContainer = document.querySelector("#vacunas-container");
const vistaFormaFisicaContainer = document.querySelector("#forma-fisica-container");
const vistaAlimentacioContainer = document.querySelector("#alimentacion-container");


vistaAvanzadaSelector.addEventListener("click", function(){
    vistaSimpleContainer.classList.add("disiplayNone");
    vistaAvanzadaContainer.classList.remove("disiplayNone");
    vistaSimpleSelector.style= ""
    vistaAvanzadaSelector.style= "color:black; background-color:white; border: black solid 2px !important;"
})

vistaSimpleSelector.addEventListener("click", function(){
    vistaAvanzadaContainer.classList.add("disiplayNone");
    vistaSimpleContainer.classList.remove("disiplayNone");
    vistaSimpleSelector.style= "color:black; background-color:white; border: black solid 2px !important;"
    vistaAvanzadaSelector.style= ""
})


vistaVacunasSelector.addEventListener("click", function(){
    vistaFormaFisicaContainer.classList.add("disiplayNone");
    vistaAlimentacioContainer.classList.add("disiplayNone");
    vistaVacunasContainer.classList.remove("disiplayNone");

    vistaFormaFisicaSelector.parentNode.classList = "selector_ofrecemos";
    vistaVacunasSelector.parentNode.classList = "selector_ofrecemos select_feedback";
    vistaAlimentacionSelector.parentNode.classList = "selector_ofrecemos";
})

vistaFormaFisicaSelector.addEventListener("click", function(){
    vistaVacunasContainer.classList.add("disiplayNone");
    vistaAlimentacioContainer.classList.add("disiplayNone");
    vistaFormaFisicaContainer.classList.remove("disiplayNone");

    vistaFormaFisicaSelector.parentNode.classList = "selector_ofrecemos select_feedback";
    vistaVacunasSelector.parentNode.classList = "selector_ofrecemos";
    vistaAlimentacionSelector.parentNode.classList = "selector_ofrecemos";

})

vistaAlimentacionSelector.addEventListener("click", function(){
    vistaVacunasContainer.classList.add("disiplayNone");
    vistaFormaFisicaContainer.classList.add("disiplayNone");
    vistaAlimentacioContainer.classList.remove("disiplayNone");

    vistaFormaFisicaSelector.parentNode.classList = "selector_ofrecemos";
    vistaVacunasSelector.parentNode.classList = "selector_ofrecemos";
    vistaAlimentacionSelector.parentNode.classList = "selector_ofrecemos select_feedback";

})