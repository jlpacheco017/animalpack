const btn_edit = document.getElementById('btn_edit');
const btn_check = document.getElementById('btn_check');
const btn_cancel = document.getElementById('btn_cancelar');
const titleInfoYear = document.getElementById('titleInfoYear');
const razaMascota = document.getElementById('razaMascota');
const titleInfoPeso = document.getElementById('titleInfoPeso');
const nameInfotitle = document.getElementById('nameInfotitle');
const usernameSelect = document.getElementById('year');
const nombreSelect = document.getElementById('razas');
const emailSelect = document.getElementById('peso');
const petName = document.getElementById('name');
const id_select = document.querySelector('.detalles-animal')


btn_edit.addEventListener("click", function(){
    btn_edit.style.display = 'none';
    titleInfoYear.style.display = 'none';
    razaMascota.style.display = 'none';
    titleInfoPeso.style.display = 'none';
    nameInfotitle.style.display = 'none';
    btn_check.style.display = '';
    btn_cancel.style.display = '';
    usernameSelect.style.display = '';
    nombreSelect.style.display = '';
    emailSelect.style.display = '';
   petName.style.display = '';
})
btn_cancel.addEventListener("click", function(){
    btn_edit.style.display = '';
    titleInfoYear.style.display = '';
    razaMascota.style.display = '';
    titleInfoPeso.style.display = '';
    nameInfotitle.style.display = '';
    btn_check.style.display = 'none';
    btn_cancel.style.display = 'none';
    usernameSelect.style.display = 'none';
    nombreSelect.style.display = 'none';
    emailSelect.style.display = 'none';
   petName.style.display = 'none';
})
btn_check.addEventListener("click", function(){
    array = [];
    const year = usernameSelect.value
    const razas = nombreSelect.value
    const peso = emailSelect.value
    const id_pet = id_select.id
    console.log(petName.value);
    const name =petName.value
    const user = {year, razas, peso, id_pet, name};
    
    fetch("/editPet", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').content // Agrega esta línea para incluir el token CSRF
        },
        body: JSON.stringify({year, razas, peso, id_pet, name})
    });
    window.location.reload();
})