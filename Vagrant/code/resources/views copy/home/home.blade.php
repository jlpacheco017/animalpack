@include('partials.header')
@extends('vistas.home')

@section('petsForeach')
    <div class="products-section">
        @foreach ($pet as $key => $pet)
            <a href="#" class="container">
                <div class="item_img">
                    <img src="{{ asset('razas/' . $pet->imagen) }}" class="img_mascotas" alt="imagen de un {{ $pet->breed }}"
                        srcset="">
                </div>
                <div>
                    <div class="petNameContainer">
                        <p>Nombre: {{ $pet->name }}
                        </p>
                    </div>
                    <div class="petInfoContainer">
                        <p>Raza: {{ $pet->breed }}
                        </p>
                    </div>
                    <div class="petInfoContainer">
                        <p>Edad: {{ $pet->age }} Años
                        </p>
                    </div>
                    <div class="petInfoContainer">
                        <p>Peso: {{ $pet->weight }} Kg
                        </p>
                    </div>
                </div>
            </a>
        @endforeach
    </div>
@endsection
@section('footer')
    @include('partials.footer')
@endsection
