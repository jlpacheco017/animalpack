<link rel="stylesheet" href="{{ asset('css/footer.css') }}">
<footer>
    <div class="container_footer">
      <div class="redes-iconos">
        <a href="#">
          <i class="fab fa-facebook-f"></i>
        </a>
        <a href="#">
          <i class="fab fa-twitter"></i>
        </a>
        <a href="#">
          <i class="fab fa-instagram"></i>
        </a>
        <a href="#">
          <i class="fab fa-youtube"></i>
        </a>
      </div>
      <div class="copy">
        © 2023 Todos los derechos reservados
      </div>
    </div>
  </footer>