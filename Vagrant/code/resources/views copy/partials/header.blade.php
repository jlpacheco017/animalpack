<link rel="stylesheet" href="{{ asset('css/header.css') }}">
<header>
    <a href="{{ route('home.home') }}"><img class="logo" src="{{ asset('img/Logo.png') }}" alt=""></a>
    <div class="item-header">
        @if (Auth::check())
            <a href="{{ route('home.home') }}" class="btn_registro"><b>INICIO</b></a>
            <p><i class="fa-solid fa-user"></i>   :   {{ session('userLogeado') }}</p>
            <a href="{{ route('auth.logOut') }}" class="btn_registro"><b>CERRAR SESION</b></a>
        @else
            <a href="{{ route('auth.showRegister') }}" class="btn_registro"><b>REGISTRARSE</b></a>
            <a href="{{ route('auth.showLogin') }}" class="btn_registro"><b>INICIAR SESION</b></a>
        @endif

    </div>
</header>
