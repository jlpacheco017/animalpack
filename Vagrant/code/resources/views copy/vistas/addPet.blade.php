<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Añadir mascota</title>
    </head>
    <body>
        <form action="" method="post" enctype="multipart/form-data">
            @csrf
            <h2>Añade tu mascota</h2>
            <label for="nombreMascota">Nombre:</label>
            <input id="nombreMascota" type="text" name="name" />
            <br />
            <label
                >Foto
                <input
                    type="file"
                    name="fotoMascota"
                    accept="image/png, image/gif, image/jpeg, image/jpg"
                />
            </label>
            <br />
            <label for="nacimientoMascota">Fecha de nacimiento:</label>
            <input id="nacimientoMascotaInput" type="date" value="" name="age" />
            <br />
            <label for="razaMascota">Raza:</label>
            <select name="breed" id="razaMascota">
                <option value="Pitbull">Pitbull</option>
                <option value="Caniche">Caniche</option>
                <option value="Pastor aleman">Pastor aleman</option>
                <option value="Beagel">Beagel</option>
            </select>
            <br />
            <label for="pesoMascota">Peso:</label>
            <input name="weight" id="pesoMascota"type="text" onkeypress="validate(event)" />
            <br />

            <input type="submit" value="Crear" />
        </form>
    </body>
</html>
