<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" />
    <title>Document</title>
</head>
<body>
    <div class="titleContainer">
        <h2>Tus mascotas</h2>
        <a href="{{ route('addPet.showAddPet') }}"<b><i class="fa-solid fa-plus"></i> Añadir Animal</b></a>
    </div>
  

    @yield('petsForeach') 
    @yield('footer') 
</body>
</html>
