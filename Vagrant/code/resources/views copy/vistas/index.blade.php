<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" />
    <title>Document</title>
</head>

<body>
    <div class="presentacion">
        <div class="presentacion_container">
            <h3>Quieres saber toda la informacion sobre tu perro, no te preocupes <span
                    style="color:#ED33B9;"><br>TENEMOS LA SOLUCION</span></h3><br>
            <a class="que_ofrecemos" href="#servicios"><b>Que Ofrecemos?</b></a>
            <!-- <button href="/registro_1.html" class="registro_btn"><b>Registrarse</b></button> -->

        </div>
        <img src="{{ asset('img/perro fondo.png') }}" alt="" srcset="">

    </div>

    <div class="perfiles">
        <h1>Enfocado a 2 perfiles</h1>
        <div class="tipos_perfiles">
            <div class="perfil_box">
                <img class="img_tipos" src="{{ asset('img/dueño.jpg') }}" alt="">
                <h2 class="titulo_tipo">DUEÑOS</h2>
                <h3 class="perfies_decripcion">Te damos las posibilidad de ver al detalle las caracteristicas de tus
                    mascotas</h3>
            </div>
            <div class="perfil_box">
                <img class="img_tipos" src="{{ asset('img/veterinario.jpg') }}" alt="">
                <h2 class="titulo_tipo">VETERINARIOS</h2>
                <h3 class="perfies_decripcion">Posibilidad de conocer todos los detalles del animal del cliente y poder
                    hacerle un diagnostico mas rápido</h3>
            </div>
        </div>
    </div>
    <div id="servicios" class="servicios">
        <h1 class="h1_servicios">Que ofrecemos?</h1>
        <div class="tipos_perfiles">
            <div class="perfil_box">
                <img class="img_tipos" src="{{ asset('img/corriendo.png') }}" alt="">
                <h2 class="titulo_tipo">RECOMENDACIONES DE MEJORA HACIA AL ANIMAL</h2>
            </div>
            <div class="perfil_box">
                <img class="img_tipos" src="{{ asset('img/peso de la comida de perro.jpg') }}" alt="">
                <h2 class="titulo_tipo">CONTROL DE PESO Y CONTROL DE ALIMENTACIÓN</h2>
            </div>

            <div class="perfil_box">
                <img class="img_tipos" src="{{ asset('img/pelota.png') }}" alt="">
                <h2 class="titulo_tipo">CONTROL DE ACTIVIDAD FÍSICA</h2>
            </div>
            <div class="perfil_box">
                <img class="img_tipos" src="{{ asset('img/vacuna.png') }}" alt="">
                <h2 class="titulo_tipo">CONTROL DE MEDICAMENTOS Y VACNAS</h2>
            </div>
        </div>
        <div>

</body>
@yield('footer')
</html>
