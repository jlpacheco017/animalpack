<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" />
    <title>Document</title>
</head>
<header>
    <a href="{{ route('home.index') }}"><img class="logo" src="{{ asset('img/Logo.png') }}" alt=""></a>
</header>

<body>
    <div class="login_superior">
        <img class="foto_login" src="{{ asset('img/perrooscuro.png') }}" srcset="">

    </div>

    <div class="login">
        <h1 class="titulo_login">INICIAR SESION</h1>

        <form  method="post">
        @csrf
            <div>
                <i id="login_icon" class="fa-solid fa-user"></i>
                <input class="login_form" type="text" id="email" name="email" placeholder="Usuario" required>

            </div>
            <div>
                <i id="login_icon" class="fa-solid fa-lock"></i>
                <input class="login_form" type="password" id="password" name="password" placeholder="Contraseña" required>
            </div>
            
            <!-- <input class="inicio_sesion_btn" type="submit" value="Iniciar sesión"> -->

             <button type="submit" class="btn btn-primary btn_avance2">
                Login
            </button>
            <a href="{{ route('auth.showRegister') }}" id="btn_volver" class="btn_volver2"><b>REGISTRARSE</b></a>
            {{-- <a href="{{route('home.home')}}" class="btn_avance"><b>INICIAR SESION</b></a>
            <a href="{{route('home.index')}}" class="btn_volver"><b>VOLVER</b></a> --}}
            </form>
        </div>
    </div>
    @yield('footer') 

</body>

</html>