<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" />
    <title>Document</title>
</head>
<header class="header_registro">
    <img class="logo" src="{{ asset('img/Logo.png') }}" alt="">
</header>

<body class="registro">

    <form  method="post">
        @csrf
        {{-- <div id="registro1" class="registro">
            <div class="registro">
                <div class="registro_progress">
                    <img class="registro_foto" src="{{ asset('img/Registro1.png') }}" srcset="">
                </div>
                <h1 class="h1_register">Para empezar necesitamos saber a que <span style="color: #ED33B9;">grupo</span>
                    perteneces?</h1>
            </div>
            <div class="registro_tipo">
                <div id="dueños" class="registro_tipo_individual">
                    <img class="img_tipos" src="{{ asset('img/dueño.jpg') }}" alt="">
                    <h1 class="h1_register">DUEÑOS</h1>

                </div>
                <div id="veterinarios" class="registro_tipo_individual">
                    <img class="img_tipos" src="{{ asset('img/veterinario.jpg') }}" alt="">
                    <h1 class="h1_register">VETERINARIO</h1>
                </div>
            </div>
            <!-- <input href="/registro_2.html" class="inicio_sesion_btn" type="submit" value="SIGUIENTE"> -->
            <button href="" id="btn_avance" class="btn_avance1"><b>SIGUIENTE</b></button>
            <button href="" id="btn_volver" class="btn_volver1"><b>VOLVER</b></button>
        </div> --}}
         



        <div id="registro2" class="registro">
        <div id="registro2" class="registro">
            <h1 class="h1_register">Necesitaremos un correo de <span style="color: #ED33B9;">contacto</span> y un
                poco de <span style="color: #ED33B9;">seguridad</span></h1>
            <div>
                <i id="login_icon" class="fa-solid fa-envelope"></i>
                <input class="login_form" type="text" id="email" name="email" placeholder="Email" required>

            </div>
            <div>
                <i id="login_icon" class="fa-solid fa-lock"></i>
                <input class="login_form" type="password" id="password" name="password" placeholder="Contraseña"
                    required>
            </div>
            {{-- <div>
                <i id="login_icon" class="fa-solid fa-lock"></i>
                <input class="login_form" type="password" id="password" name="password"
                    placeholder="Repite la Contraseña" required>
            </div> --}}

            <!-- <input href="/registro_3.html" class="inicio_sesion_btn" type="submit" value="Siguiente"> -->
            {{-- <button href="" id="btn_avance" class="btn_avance2"><b>SIGUIENTE</b></button> --}}
             <button type="submit" id="btn_avance" class="btn btn-primary btn_avance2">Registrarse</button>
             <a href="{{ route('home.index') }}" id="btn_volver" class="btn_volver2"><b>Volver</b></a>
             <a href="{{ route('auth.showLogin') }}" class="btn_avance2"><b>LOGIN</b></a>
        </div>



        {{-- <div id="registro3" class="oculto">
            <div class="registro_progress">
                <img class="registro_foto" src="{{ asset('img/Regitro3.png') }}" srcset="">
            </div>
            <h1 class="h1_register">Para finalizar necesitamos saber cual es tu <span
                    style="color: #ED33B9;">Nombre</span>. </h1>
            <div>
                <i id="login_icon" class="fa-solid fa-user"></i>
                <input class="login_form" type="text" id="username" name="username" placeholder="Nombre" required>

            </div>
            <div>
                <i id="login_icon" class="fa-solid fa-user"></i>
                <input class="login_form" type="password" id="password" name="password" placeholder="Apellido"
                    required>
            </div>
            <div>
                <i id="login_icon" class="fa-solid fa-phone"></i>
                <input class="login_form" type="password" id="password" name="password" placeholder="Telefono"
                    required>
            </div> --}}

            <!-- <input href="/index.html" class="inicio_sesion_btn" type="submit" value="Finalizar"> -->
            {{-- <button href="" id="btn_avance" class="btn_avance3"><b>FINALIZAR</b></button>
            <button href="" id="btn_volver" class="btn_volver3"><b>VOLVER</b></button>
        </div> --}}
    </form>


</body>
</html>
