@extends('layouts.master')
@section('title', 'AnimalPack - HOME')
@section('content')
    <div class="index_content">
        <div class="titleContainer">
            <h2>Tus mascotas</h2>
            <a href="{{ route('addPet.showAddPet') }}"><b><i class="fa-solid fa-plus"></i> Añadir Animal</b></a>
        </div>
        <div class="products-section">
            @error('error')
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $message }}
                </div>
            @enderror
            @foreach ($pet as $key => $pet)
                <a href="{{ route('petDetail.showPetDetail', ['id' => $pet->id]) }}" class="container">
                    <div class="item_img">
                        <img src="{{ asset('img/pets/' . $pet->imagen) }}" class="img_mascotas"
                            alt="imagen de un {{ $pet->breed }}" srcset="">
                    </div>
                    <div>
                        <div class="petNameContainer">
                            <p><b>Nombre: </b>{{ $pet->name }}
                            </p>
                        </div>
                        <div class="petNameContainer">
                            <p><b>Raza: </b>{{ $pet->breed }}
                            </p>
                        </div>
                        <?php
                        $cumpleaños = \Carbon\Carbon::parse($pet->age);
                        $fechaDeHoy = \Carbon\Carbon::now();
                        $años = $cumpleaños->diffInYears($fechaDeHoy);
                        ?>
                        <div class="petNameContainer">
                            <p><b>Edad: </b>{{ $años }} Años</p>
                        </div>

                        <div class="petNameContainer">
                            <p><b>Peso: </b>{{ $pet->weight }} Kg
                            </p>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
@endsection
