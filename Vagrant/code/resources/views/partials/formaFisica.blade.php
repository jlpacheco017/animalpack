@if ($physicalActivity->first() != null)
    <table class="formaFisicaTable">
        <tr>
            <td>
                Fecha inicio
            </td>
            <td>
                Fecha final
            </td>
            <td>
                Tiempo
            </td>
        </tr>
        @foreach ($physicalActivity as $pa)
            <tr class="userInfo">
                <td>
                    {{ date_create($pa->started_at)->format('d-m-Y H:i:s') }}
                </td>
                <td>
                    {{ date_create($pa->finished_at)->format('d-m-Y H:i:s') }}
                </td>
                <td>
                    {{ date_create($pa->started_at)->diff(date_create($pa->finished_at))->format('%H:%I:%S') }}
                </td>
            </tr>
        @endforeach
    </table>
    <div class="totalTimeContainer">
        <p class="physicalActivitiesRecomendedTime">Te recomendadmos al rededor de <span
                class="userInfo">{{ $recomendedTime }}</span> minutos de actividad física al dia para la raza de tu
            perro.</p>
        <p class="physicalActivitiesTotalTime">Total: <span class="userInfo">{{ $time }}</span></p>
    </div>
@else
    <p class="physicalError">No hay actividad física registrada</p>
@endif
