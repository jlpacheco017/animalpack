<header>
    
        @if (Auth::check())
        <a href="{{ route('home.home') }}"><img class="logo" src="{{ asset('img/Logo.png') }}" alt=""></a>
        <div class="item-header">
            @if (Auth::user()->subscripcion == "Free")
            <a href={{ route('animal.planes') }} class="btn_header header_subcripcion-free"><b><i class="fa-solid fa-burst"></i>Free</b></a>
            @endif
            @if (Auth::user()->subscripcion == "Basic")
            
            <a href={{ route('animal.planes') }} class=" btn_header header_subcripcion-basic"><b><i class="fa-solid fa-burst"></i> Basic <i class="fa-solid fa-burst"></i></b></a>
            @endif
            @if (Auth::user()->subscripcion == "Plus")
            
            <a href={{ route('animal.planes') }} class=" btn_header header_subcripcion-plus"><b><i class="fa-solid fa-burst"></i> Plus <i class="fa-solid fa-burst"></i></b></a>
            @endif
            @if (Auth::user()->subscripcion == "Premium")
            <a href={{ route('animal.planes') }} class="btn_header header_subcripcion-premium"><b><i class="fa-solid fa-burst"></i> Premium <i class="fa-solid fa-burst"></i></b></a>
            
            @endif
            <a href="{{ route('userDetail.showUserDetail') }}" class="btn_header"><b class="userLoggeado"><i class="fa-solid fa-user-large"></i>   :   {{ session('userLogeado') }}</b></a>
            <a href="{{ route('auth.logOut') }}" class="btn_header"><b>CERRAR SESION</b></a>
        </div>
        @else
        <a href="{{ route('home.index') }}"><img class="logo" src="{{ asset('img/Logo.png') }}" alt=""></a>
        <div class="item-header">
            <a href="{{ route('auth.showRegister') }}" class="btn_header"><b>REGISTRARSE</b></a>
            <a href="{{ route('auth.showLogin') }}" class="btn_header"><b>INICIAR SESION</b></a>
        </div>
        @endif

    
</header>
