@extends('layouts.master')
@section('title', 'AnimalPack - Detalles')
@section('content')
    @if ($pet)
        <div id="container-detalles-animal">
            <div id="{{ $pet->id }}" class="detalles-animal">
                <div class="pet-details-right-div">
                    <div class="pet-details-name-container">
                        <h1 class="dog_name" id="nameInfotitle">{{ $pet->name }}</h1>
                        <input style="display: none;" class="login_form" type="text" id="name" name="name"
                            value="{{ $pet->name }}">
                    </div>

                    <div class="pet-details-type-info-selector">
                        <div class="info-selector">
                            <p id="simple-info-selector" style="color:black; background-color:white; border: black solid 2px !important;">Informacion simple</p>
                        </div>
                        <div class="pet-details-pet-image-container">
                            <img src="{{ asset('img/pets/' . $pet->imagen) }}" alt="{{ $pet->name }}"
                                class="pet-detail-image">
                        </div>
                        <div class="info-selector">
                            <p id="advanced-info-selector">Informacion avanzada</p>
                        </div>
                    </div>
                    <div id="simple-info-container" class="pet-details-pet-info-container" style="">
                        <?php
                        $cumpleaños = \Carbon\Carbon::parse($pet->age);
                        $fechaDeHoy = \Carbon\Carbon::now();
                        $años = $cumpleaños->diffInYears($fechaDeHoy);
                        ?>

                        <div class="userNameContainer">
                            <p class="titleInfo">Nacimiento:</p>
                            <p class="userInfo" id="titleInfoYear">{{ $pet->age }}</p>
                            <input class="login_form" style="display: none;" id="year" type="date"
                                value="{{ $pet->age }}" name="age" />
                        </div>
                        <div class="userNameContainer">
                            <p class="titleInfo">Edad:</p>
                            <p class="userInfo" id="titleInfoNombre">{{ $años }} Años</p>
                        </div>
                        <div class="userNameContainer">
                            <p class="titleInfo">Raza:</p>
                            <p class="userInfo" id="razaMascota">{{ $pet->breed }}</p>
                            <select style="display: none; height: 3rem; border-radius: 10px;" name="breed" id="razas">
                                <option class="login_form" value="{{ $pet->breed }}" disabled selected>
                                    {{ $pet->breed }}
                                </option>
                                @foreach ($razas as $key => $raza)
                                    <option value="{{ $raza->raza }}">{{ $raza->raza }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="userNameContainer">
                            <p class="titleInfo">Peso:</p>
                            <p class="userInfo" id="titleInfoPeso">{{ $pet->weight }} Kg</p>
                            <input style="display: none;" class="login_form" type="text" id="peso" name="username"
                                value="{{ $pet->weight }}">
                        </div>
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <div class="error-button-container">
                            <div class="edit-profile">

                                <button id="btn_edit" class="btn btn-primary btn_avance2">Editar <i class="fa-solid fa-pen"
                                        style="color: #000000;"></i></button>
                                <button id="btn_check" style="display: none;"><i class="fa-regular fa-circle-check"
                                        style="color: #57ae37; font-size: 2rem;"></i></button>
                                <button id="btn_cancelar" style="display: none;"><i
                                        class="fa-sharp fa-regular fa-circle-xmark"
                                        style="color: #a84038; font-size: 2rem;"></i></button>
                            </div>
                            <div>
                                @if ($errors->has('name'))
                                    <div class="error-message errro-details">
                                        <i class='bx bx-error'></i> {{ $errors->first('name') }}
                                    </div>
                                @endif
                                @if ($errors->has('razas'))
                                    <div class="error-message errro-details">
                                        <i class='bx bx-error'></i> {{ $errors->first('razas') }}
                                    </div>
                                @endif
                                @if ($errors->has('peso'))
                                    <div class="error-message errro-details">
                                        <i class='bx bx-error'></i> {{ $errors->first('peso') }}
                                    </div>
                                @endif
                                @if ($errors->has('year'))
                                    <div class="error-message errro-details">
                                        <i class='bx bx-error'></i> {{ $errors->first('year') }}
                                    </div>
                                @endif
                            </div>


                        </div>

                    </div>

                    <div id="advanced-info-container" class="pet-details-pet-info-container advanced-selector disiplayNone">
                        <div class="pet-details-type-info-selector advanced-info-selector">
                            <div class="selector_ofrecemos select_feedback">
                                <p id="vista-vacunas-selector" >Vacunas</p>
                            </div>
                            <div class="selector_ofrecemos">
                                <p id="vista-forma-fisisca-selector">Forma física</p>
                            </div>
                            <div class="selector_ofrecemos">
                                <p id="vista-alimentacion-selector">Alimentación</p>
                            </div>
                        </div>
                        <div class="advanced-info">
                            <div id="vacunas-container">
                                <p>a</p>
                            </div>
                            <div id="forma-fisica-container" class="disiplayNone">
                                <p>@include('partials.formaFisica')</p>
                            </div>
                            <div id="alimentacion-container" class="disiplayNone">
                                <p>@include('partials.pesoComida')</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif
@endsection
@section('js')
    <script src="/js/editPet.js"></script>
    <script src="/js/changePetDetailsView.js"></script>
@endsection
