<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\authController;
use App\Http\Controllers\homeController;
use App\Http\Controllers\userController;
use App\Http\Controllers\AddPetController;
use App\Http\Controllers\PlanesController;
use App\Http\Controllers\petDetailsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [homeController::class, 'index'])->name('home.index');
Route::get('/home', [homeController::class, 'home'])->name('home.home')->middleware('auth');

Route::get('/login', [authController::class, 'showLogin'])->name('auth.showLogin');
Route::post('/login', [authController::class, 'doLogin'])->name('auth.doLogin'); 

Route::get('/register', [authController::class, 'showRegister'])->name('auth.showRegister');
Route::post('/register', [authController::class, 'doRegister'])->name('auth.doRegister'); 

Route::get('/addPet', [AddPetController::class, 'showAddPet'])->name('addPet.showAddPet')->middleware('auth');
Route::post('/addPet', [AddPetController::class, 'doAddPet'])->name('addPet.doAddPet')->middleware('auth'); 


Route::get('/petDetails/{id}', [petDetailsController::class, 'showPetDetail'])->name('petDetail.showPetDetail')->middleware('auth');
Route::post('/editPet', [petDetailsController::class, 'editPet'])->name('petDetail.editPet')->middleware('auth');

Route::get('dashboard', [authController::class, 'dashboard']); 
Route::get('logout', [authController::class, 'logout'])->name('auth.logOut');

Route::get('/user', [userController::class, 'showUserDetail'])->name('userDetail.showUserDetail')->middleware('auth');
Route::post('/editar-perfil', [userController::class, 'editUserDetail'])->name('userDetail.editUserDetail')->middleware('auth');

Route::get('/planes', [PlanesController::class, 'showPlanes'])->name('animal.planes'); 
Route::post('/planes-cancel', [PlanesController::class, 'cancelSub'])->name('animal.cancelSub'); 
Route::post('/planes-new', [PlanesController::class, 'newSub'])->name('animal.newSub'); 
