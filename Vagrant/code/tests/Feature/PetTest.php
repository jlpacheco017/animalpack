<?php

namespace Tests\Feature;

use App\Models\Pets;
use App\Models\Razas;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Database\Seeders\RazasTableSeeder;

class PetTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_añadir_animal(): void
    {
        User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        // ENTRAMOS CON EL USUARIO CREADO.
        $user = User::find(1);
        // Entrar con el usuario creado.
        $this->actingAs($user);
    
        $this->post(route('addPet.doAddPet'), [
            'name' => 'Max',
            'age' => "2023-05-17",
            'breed' => 'Doberman Pinscher',
            'weight' => "20"
        ]);
        $this->assertDatabaseHas('pets', ['name' => 'Max', 'age' => "2023-05-17", 'breed' => 'Doberman Pinscher']);
    
    }

    public function test_mensaje_error_fecha_superior_al_dia_de_hoy(): void
    {
        User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        // ENTRAMOS CON EL USUARIO CREADO.
        $user = User::find(1);
        // Entrar con el usuario creado.
        $this->actingAs($user);
    
        $response = $this->post(route('addPet.doAddPet'), [
            'name' => 'Max',
            'age' => "2024-05-27",
            'breed' => 'Doberman Pinscher',
            'weight' => "20"
        ]);
        $response->assertSessionHasErrors([
            'age' => 'La fecha no puede ser superior a hoy.'
        ]);
    
    }

    public function test_mensaje_error_fecha_superior_a_20_años(): void
    {
        User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        // ENTRAMOS CON EL USUARIO CREADO.
        $user = User::find(1);
        // Entrar con el usuario creado.
        $this->actingAs($user);
    
        $response = $this->post(route('addPet.doAddPet'), [
            'name' => 'Max',
            'age' => "2000-05-27",
            'breed' => 'Doberman Pinscher',
            'weight' => "20"
        ]);
        $response->assertSessionHasErrors([
            'age' => 'La edad no puede ser mayor a 20 años.',
        ]);
    
    }

    public function test_mensaje_error_peso_vacio(): void
    {
        User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        // ENTRAMOS CON EL USUARIO CREADO.
        $user = User::find(1);
        // Entrar con el usuario creado.
        $this->actingAs($user);
    
        $response = $this->post(route('addPet.doAddPet'), [
            'name' => 'Max',
            'age' => "2000-05-27",
            'breed' => 'Doberman Pinscher',
            'weight' => null
        ]);
        $response->assertSessionHasErrors([
            'weight' => 'El peso es obligatorio.',
        ]);
    
    }
    public function test_mensaje_error_raza_vacio(): void
    {
        User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        // ENTRAMOS CON EL USUARIO CREADO.
        $user = User::find(1);
        // Entrar con el usuario creado.
        $this->actingAs($user);
    
        $response = $this->post(route('addPet.doAddPet'), [
            'name' => 'Max',
            'age' => "2000-05-27",
            'breed' => null,
            'weight' => "23"
        ]);
        $response->assertSessionHasErrors([
            'breed' => 'La raza es obligatoria.',
        ]);
    
    }

    public function test_entrar_en_animal_no_tuyo_y_mustra_error(): void
    {
        User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        User::factory()->create([
            'username' => 'dfernandez',
            'nombre' => 'Diego',
            'email' => 'dfernandez@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        // ENTRAMOS CON EL USUARIO CREADO.
        $user = User::find(1);
        // Entrar con el usuario creado.
        $this->actingAs($user);
    
        $this->post(route('addPet.doAddPet'), [
            'name' => 'Max',
            'age' => "2023-05-17",
            'breed' => 'Doberman Pinscher',
            'weight' => "20"
        ]);

        $user = User::find(2);
        // Entrar con el usuario creado.
        $this->actingAs($user);
    
        $this->post(route('addPet.doAddPet'), [
            'name' => 'Max',
            'age' => "2023-05-17",
            'breed' => 'Doberman Pinscher',
            'weight' => "20"
        ]);

        $response = $this->get(route('petDetail.showPetDetail', ['id' => 1]));
        $response->assertSessionHasErrors([
            'error' => 'No tienes acceso al animal seleccionado o no esta disponible su información',
        ]);
    
    }

    public function test_mensaje_error_fecha_superior_al_dia_de_hoy_en_editar_animal(): void
    {
        $this->seed(RazasTableSeeder::class);
        User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        // ENTRAMOS CON EL USUARIO CREADO.
        $user = User::find(1);
        // Entrar con el usuario creado.
        $this->actingAs($user);
    
        $this->post(route('addPet.doAddPet'), [
            'name' => 'Max',
            'age' => "2022-05-27",
            'breed' => 'Doberman Pinscher',
            'weight' => "20"
        ]);
        $this->get(route('petDetail.showPetDetail', ['id' => 1]));

        $response = $this->post(route('petDetail.editPet'), [
            'name' => 'Max',
            'year' => "2024-05-27",
            'razas' => 'Doberman Pinscher',
            'peso' => "20",
            'id_pet' => 1
        ]);
        
        $response->assertSessionHasErrors('year', 'La fecha no puede ser superior a hoy.');
    
    }

    public function test_mensaje_error_fecha_superior_a_20_años_en_editar_animal(): void
    {
        User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        // ENTRAMOS CON EL USUARIO CREADO.
        $user = User::find(1);
        // Entrar con el usuario creado.
        $this->actingAs($user);
    
        $this->post(route('addPet.doAddPet'), [
            'name' => 'Max',
            'age' => "2022-05-27",
            'breed' => 'Doberman Pinscher',
            'weight' => "20"
        ]);
        $this->get(route('petDetail.showPetDetail', ['id' => 1]));

        $response = $this->post(route('petDetail.editPet'), [
            'name' => 'Max',
            'year' => "2000-05-27",
            'razas' => 'Doberman Pinscher',
            'peso' => "20",
            'id_pet' => 1
        ]);

        $response->assertSessionHasErrors([
            'year' => 'La edad no puede ser mayor a 20 años.',
        ]);
    
    }

    public function test_mensaje_error_peso_vacio_en_editar_animal(): void
    {
 User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        // ENTRAMOS CON EL USUARIO CREADO.
        $user = User::find(1);
        // Entrar con el usuario creado.
        $this->actingAs($user);
    
        $this->post(route('addPet.doAddPet'), [ 
            'name' => 'Max',
            'age' => "2022-05-27",
            'breed' => 'Doberman Pinscher',
            'weight' => "20"
        ]);
        $this->get(route('petDetail.showPetDetail', ['id' => 1]));

        $response = $this->post(route('petDetail.editPet'), [
            'name' => 'Max',
            'year' => "2023-05-26",
            'razas' => 'Doberman Pinscher',
            'peso' => null,
            'id_pet' => 1
        ]);

        $response->assertSessionHasErrors([
            'peso' => 'El peso es obligatorio.',
        ]);
    
    }
    public function test_mensaje_error_raza_vacio_en_editar_animal(): void
    {
        User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        // ENTRAMOS CON EL USUARIO CREADO.
        $user = User::find(1);
        // Entrar con el usuario creado.
        $this->actingAs($user);
    
$this->post(route('addPet.doAddPet'), [
            'name' => 'Max',
            'age' => "2022-05-27",
            'breed' => 'Doberman Pinscher',
            'weight' => "20"
        ]);
        $this->get(route('petDetail.showPetDetail', ['id' => 1]));

        $response = $this->post(route('petDetail.editPet'), [
            'name' => 'Max',
            'year' => "2024-05-27",
            'peso' => "20",
            'id_pet' => 1
        ]);

        $response->assertSessionHasErrors([
            'razas' => 'La raza es obligatoria.',
        ]);
    
    }
}
