<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Pets;
use App\Models\User;
use App\Models\Razas;
use App\Models\PhysicalActivity;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PhysicalActivitiesTest extends TestCase
{
    /**
     * A basic feature test example.
     */

    use RefreshDatabase;

    public function test_anadir_physical_activities(): void
    {
        //Creamos un usuario
        User::factory()->create([
            'username' => 'adrian',
            'nombre' => 'adrian',
            'email' => 'adri@test.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);

        //Creamos una mascota
        $pet = Pets::factory()->create([
            "name" => "TestPet",
            "imagen" => "testImagen.jpg",
            "age" => "2023-05-27",
            "breed" => "West Highland White Terrier",
            "weight" => 18.20,
            "user_id" => 1
        ]);

        //Creamos los datos necesarios para crear una actividad física
        $fecha = date_create();
        $fecha2 = clone $fecha;
        date_modify($fecha2, '+' . random_int(0, 60) . ' minute');
        $startDate = $fecha;
        $endDate = $fecha2;

        //Creamos la actividad fisica
        PhysicalActivity::factory()->create([
            'started_at' => $startDate,
            'finished_at' => $endDate,
            'pet_id' => $pet->id
        ]);

        //Verificamos que los datos se han guardado en base de datos y son correctos
        $this->assertDatabaseHas('physical_activities', ['started_at' => $startDate, 'finished_at' => $endDate, 'pet_id' => $pet->id]);
    }

    public function test_actividad_fisca_con_id_de_animal(): void
    {        
        //Creamos un usuario
        User::factory()->create([
            'username' => 'adrian',
            'nombre' => 'adrian',
            'email' => 'adri@test.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);

        //Creamos una mascota
        $pet = Pets::factory()->create([
            "name" => "TestPet",
            "imagen" => "testImagen.jpg",
            "age" => "2023-05-27",
            "breed" => "West Highland White Terrier",
            "weight" => 18.20,
            "user_id" => 1
        ]);

        //Creamos los datos necesarios para crear una actividad física
        $fecha = date_create();
        $fecha2 = clone $fecha;
        date_modify($fecha2, '+' . random_int(0, 60) . ' minute');
        $startDate = $fecha;
        $endDate = $fecha2;

        //Creamos la actividad fisica
        PhysicalActivity::factory()->create([
            'started_at' => $startDate,
            'finished_at' => $endDate,
            'pet_id' => $pet->id
        ]);

        //Llamamps a la función y nos guardamos lo que devuelve
        $testPhyAct = PhysicalActivity::physycalActvityFromPet($pet->id);

        //Comprovamos si lo que nos ha devuelto la función está guardado en base de datos
        $this->assertDatabaseHas('physical_activities', ['started_at' => $testPhyAct->first()->started_at, 'finished_at' => $testPhyAct->first()->finished_at, 'pet_id' => $pet->id]);
    }

    public function test_actividad_recomendada_fisca_con_id_de_animal(): void
    {
        User::factory()->create([
            'username' => 'adrian',
            'nombre' => 'adrian',
            'email' => 'adri@test.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);

        $pet = Pets::factory()->create([
            "name" => "TestPet",
            "imagen" => "testImagen.jpg",
            "age" => "2023-05-27",
            "breed" => "Labrador Retriever",
            "weight" => 18.20,
            "user_id" => 1
        ]);

        $breed = new Razas([
            'raza' => 'Labrador Retriever',
            'pmin' => 25,
            'pmax' => 36,
            'comidamin' => 360,
            'comidamed' => 480,
            'comidamax' => 600,
            'recomendedTime' => 60
        ]);
        $breed->save();

        $recomendedTime = PhysicalActivity::recomendedPhysicalActivity($pet->id);

        $this->assertEquals($breed->recomendadTime, $recomendedTime);
    }
}
