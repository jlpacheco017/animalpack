<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;
class RegisterTest extends TestCase
{
use RefreshDatabase;
    public function test_registrar_un_usuario_correctamente()
    {
        $response = $this->post(Route('auth.doRegister'), [
            'username' => 'shadow2',
            'email' => 'shadow2@gmail.com',
            'name' => 'Shadow Shooter',
            'password' => 'Jzk9P8XZ1',
        ]);

        // COMPROBAR QUE HACE EL REDIRECT AL CREARLO.
        $response->assertRedirect(Route('auth.showLogin'));

        // COMPROBAR HA SIDO INSERTADO EN LA BBDD.
        $this->assertDatabaseHas('users', [
            'username' => 'shadow2',
            'email' => 'shadow2@gmail.com',
        ]);
    }

    public function test_registrar_un_usuario_con_tildes()
    {
        // CREAR UN USUARIO CON TILDES.
        $response = $this->post(Route('auth.doRegister'), [
            'username' => 'jlpachéco',
            'name' => 'Jose Luis',
            'email' => 'jlpachéco@gmail.com',
            'password' => 'Jzk9P8XZ1'
        ]);

        // COMPROBAR QUE HACE EL REDIRECT AL CREARLO.
        $response->assertRedirect(Route('auth.showLogin'));

        // COMPROBAR HA SIDO INSERTADO EN LA BBDD.
        $this->assertDatabaseHas('users', [
            'username' => 'jlpachéco',
            'email' => 'jlpachéco@gmail.com'
        ]);
    }

   

    public function test_registrar_un_usuario_con_correo_invalido()
    {
        // CREAR UN USUSARIO CON UN EMAIL INVALIDO.
        $response = $this->post(Route('auth.doRegister'), [
            'username' => 'JordiLordanBurgos',
            'name' => 'Jordi Lordan',
            'email' => 'jør∂î.lør∂añ@copernic@copernic.cat',
            'password' => 'Jzk9P8XZ1',
        ]);

        // COMPROBAR QUE NOS DA UN ERROR DE 'EMAIL'.
        $response->assertSessionHasErrors('email');

        // COMPROBAR QUE NO HA SIDO INSERTADO EN LA BBDD.
        $this->assertDatabaseMissing('users', [
            'username' => 'JordiLordanBurgos',
            'email' => 'jør∂î.lør∂añ@copernic@copernic.cat',
        ]);
    }

    public function test_registrar_un_usuario_con_un_correo_que_ya_existe()
    {
        // CREAR UN CORREO.
        User::factory()->create([
            'email' => 'estecorreo@yaexiste.com',
        ]);

        // CREAR UN USUARIO CON ESE MISMO CORREO.
        $response = $this->post(Route('auth.doRegister'), [
            'username' => 'jlpacheco',
            'name' => 'Jose Luis',
            'email' => 'estecorreo@yaexiste.com',
            'password' => 'Jzk9P8XZ1',
        ]);

        // COMPROBAR QUE DA ERROR TRAS INTENTAR CREAR UN USUARIO CON EL MISMO CORREO
        $response->assertSessionHasErrors('email');

        // COMPROBAR QUE NO HA SIDO INSERTADO EN LA BBDD.
        $this->assertDatabaseMissing('users', [
            'username' => 'jlpacheco',
            'email' => 'estecorreo@yaexiste.com',
        ]);
    }

    public function test_registrar_un_usuario_con_una_password_debil()
    {
        // CREAR UN USUARIO CON UNA CONTRASEÑA DÉBIL.
        $response = $this->post(Route('auth.doRegister'), [
            'username' => 'DiegoFernandezPerez',
            'name' => 'Diego Fernandez',
            'email' => 'fernandez.perez.diego@alumnat.copernic.cat',
            'password' => 'contraseña_segura'
        ]);

        // COMPROBAR QUE NOS DA UN ERROR DE 'PASSWORD'.
        $response->assertSessionHasErrors('password');

        // COMPROBAR QUE NO HA SIDO INSERTADO EN LA BBDD.
        $this->assertDatabaseMissing('users', [
            'username' => 'DiegoFernandezPerez',
            'email' => 'fernandez.perez.diego@alumnat.copernic.cat',
        ]);
    }
}
