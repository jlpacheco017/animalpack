<header>
    
        <?php if(Auth::check()): ?>
        <a href="<?php echo e(route('home.home')); ?>"><img class="logo" src="<?php echo e(asset('img/Logo.png')); ?>" alt=""></a>
        <div class="item-header">
            <?php if(Auth::user()->subscripcion == "Free"): ?>
            <a href=<?php echo e(route('animal.planes')); ?> class="btn_header header_subcripcion-free"><b><i class="fa-solid fa-burst"></i>Free</b></a>
            <?php endif; ?>
            <?php if(Auth::user()->subscripcion == "Basic"): ?>
            
            <a href=<?php echo e(route('animal.planes')); ?> class=" btn_header header_subcripcion-basic"><b><i class="fa-solid fa-burst"></i> Basic <i class="fa-solid fa-burst"></i></b></a>
            <?php endif; ?>
            <?php if(Auth::user()->subscripcion == "Plus"): ?>
            
            <a href=<?php echo e(route('animal.planes')); ?> class=" btn_header header_subcripcion-plus"><b><i class="fa-solid fa-burst"></i> Plus <i class="fa-solid fa-burst"></i></b></a>
            <?php endif; ?>
            <?php if(Auth::user()->subscripcion == "Premium"): ?>
            <a href=<?php echo e(route('animal.planes')); ?> class="btn_header header_subcripcion-premium"><b><i class="fa-solid fa-burst"></i> Premium <i class="fa-solid fa-burst"></i></b></a>
            
            <?php endif; ?>
            <a href="<?php echo e(route('userDetail.showUserDetail')); ?>" class="btn_header"><b class="userLoggeado"><i class="fa-solid fa-user-large"></i>   :   <?php echo e(session('userLogeado')); ?></b></a>
            <a href="<?php echo e(route('auth.logOut')); ?>" class="btn_header"><b>CERRAR SESION</b></a>
        </div>
        <?php else: ?>
        <a href="<?php echo e(route('home.index')); ?>"><img class="logo" src="<?php echo e(asset('img/Logo.png')); ?>" alt=""></a>
        <div class="item-header">
            <a href="<?php echo e(route('auth.showRegister')); ?>" class="btn_header"><b>REGISTRARSE</b></a>
            <a href="<?php echo e(route('auth.showLogin')); ?>" class="btn_header"><b>INICIAR SESION</b></a>
        </div>
        <?php endif; ?>

    
</header>
<?php /**PATH C:\Users\jlpacheco\Documents\animalpack\code\resources\views/partials/header.blade.php ENDPATH**/ ?>