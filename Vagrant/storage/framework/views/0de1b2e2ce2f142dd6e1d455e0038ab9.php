
<?php $__env->startSection('title', 'AnimalPack - HOME'); ?>
<?php $__env->startSection('content'); ?>
    <div class="userDetail_container">

        <div class="user-section">
            <h2>INFORMACION DEL USUARIO</h2>
            <div class="userNameContainer">
                <?php if(session('validation')): ?>
                    <div class="error-message"><?php echo e(session('validation')); ?></div>
                <?php endif; ?>
                <?php if(session('succes')): ?>
                    <div class="succes-message"><?php echo e(session('succes')); ?></div>
                <?php endif; ?>
                <p class="titleInfo">Nombre de usuario</p>
                <p class="userInfo" id="titleInfoUsername"><?php echo e($user->username); ?></p>
                <input style="display: none;" class="login_form" type="text" id="username" name="username"
                    value="<?php echo e($user->username); ?>">
            </div>
            
            <div class="userNameContainer">
                <p class="titleInfo">Nombre</p>
                <p class="userInfo" id="titleInfoNombre"><?php echo e($user->nombre); ?></p>
                <input style="display: none;" class="login_form" type="text" id="nombre" name="username"
                    value="<?php echo e($user->nombre); ?>">
            </div>
            <div class="userNameContainer">
                <p class="titleInfo">Email</p>
                <p class="userInfo" id="titleInfoEmail"><?php echo e($user->email); ?></p>
                <input style="display: none;" class="login_form" type="text" id="email" name="username"
                    value="<?php echo e($user->email); ?>">
            </div>
            <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
            <div class="edit-profile">
                <button id="btn_edit" class="btn btn-primary btn_avance2">Editar <i class="fa-solid fa-pen" style="color: #000000;"></i></button>
                <button id="btn_check" style="display: none;"><i class="fa-regular fa-circle-check"
                        style="color: #57ae37; font-size: 2rem;"></i></button>
                <button id="btn_cancelar" style="display: none;"><i class="fa-sharp fa-regular fa-circle-xmark"
                        style="color: #a84038; font-size: 2rem;"></i></button>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="/js/editInfo.js"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\jlpacheco\Documents\animalpack\code\resources\views/user/userDetail.blade.php ENDPATH**/ ?>