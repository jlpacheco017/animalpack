<?php if($physicalActivity->first() != null): ?>
    <table class="formaFisicaTable">
        <tr>
            <td>
                Fecha inicio
            </td>
            <td>
                Fecha final
            </td>
            <td>
                Tiempo
            </td>
        </tr>
        <?php $__currentLoopData = $physicalActivity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pa): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr class="userInfo">
                <td>
                    <?php echo e(date_create($pa->started_at)->format('d-m-Y H:i:s')); ?>

                </td>
                <td>
                    <?php echo e(date_create($pa->finished_at)->format('d-m-Y H:i:s')); ?>

                </td>
                <td>
                    <?php echo e(date_create($pa->started_at)->diff(date_create($pa->finished_at))->format('%H:%I:%S')); ?>

                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>
    <div class="totalTimeContainer">
        <p class="physicalActivitiesRecomendedTime">Te recomendadmos al rededor de <span
                class="userInfo"><?php echo e($recomendedTime); ?></span> minutos de actividad física al dia para la raza de tu
            perro.</p>
        <p class="physicalActivitiesTotalTime">Total: <span class="userInfo"><?php echo e($time); ?></span></p>
    </div>
<?php else: ?>
    <p class="physicalError">No hay actividad física registrada</p>
<?php endif; ?>
<?php /**PATH C:\Users\jlpacheco\Documents\animalpack\code\resources\views/partials/formaFisica.blade.php ENDPATH**/ ?>