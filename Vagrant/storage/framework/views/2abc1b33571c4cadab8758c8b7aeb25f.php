
<?php $__env->startSection('title', 'AnimalPack - Detalles'); ?>
<?php $__env->startSection('content'); ?>
    <?php if($pet): ?>
        <div id="container-detalles-animal">
            <div id="<?php echo e($pet->id); ?>" class="detalles-animal">
                <div class="pet-details-right-div">
                    <div class="pet-details-name-container">
                        <h1 class="dog_name" id="nameInfotitle"><?php echo e($pet->name); ?></h1>
                        <input style="display: none;" class="login_form" type="text" id="name" name="name"
                            value="<?php echo e($pet->name); ?>">
                    </div>

                    <div class="pet-details-type-info-selector">
                        <div class="info-selector">
                            <p id="simple-info-selector" style="color:black; background-color:white; border: black solid 2px !important;">Informacion simple</p>
                        </div>
                        <div class="pet-details-pet-image-container">
                            <img src="<?php echo e(asset('img/pets/' . $pet->imagen)); ?>" alt="<?php echo e($pet->name); ?>"
                                class="pet-detail-image">
                        </div>
                        <div class="info-selector">
                            <p id="advanced-info-selector">Informacion avanzada</p>
                        </div>
                    </div>
                    <div id="simple-info-container" class="pet-details-pet-info-container" style="">
                        <?php
                        $cumpleaños = \Carbon\Carbon::parse($pet->age);
                        $fechaDeHoy = \Carbon\Carbon::now();
                        $años = $cumpleaños->diffInYears($fechaDeHoy);
                        ?>

                        <div class="userNameContainer">
                            <p class="titleInfo">Nacimiento:</p>
                            <p class="userInfo" id="titleInfoYear"><?php echo e($pet->age); ?></p>
                            <input class="login_form" style="display: none;" id="year" type="date"
                                value="<?php echo e($pet->age); ?>" name="age" />
                        </div>
                        <div class="userNameContainer">
                            <p class="titleInfo">Edad:</p>
                            <p class="userInfo" id="titleInfoNombre"><?php echo e($años); ?> Años</p>
                        </div>
                        <div class="userNameContainer">
                            <p class="titleInfo">Raza:</p>
                            <p class="userInfo" id="razaMascota"><?php echo e($pet->breed); ?></p>
                            <select style="display: none; height: 3rem; border-radius: 10px;" name="breed" id="razas">
                                <option class="login_form" value="<?php echo e($pet->breed); ?>" disabled selected>
                                    <?php echo e($pet->breed); ?>

                                </option>
                                <?php $__currentLoopData = $razas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $raza): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($raza->raza); ?>"><?php echo e($raza->raza); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="userNameContainer">
                            <p class="titleInfo">Peso:</p>
                            <p class="userInfo" id="titleInfoPeso"><?php echo e($pet->weight); ?> Kg</p>
                            <input style="display: none;" class="login_form" type="text" id="peso" name="username"
                                value="<?php echo e($pet->weight); ?>">
                        </div>
                        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
                        <div class="error-button-container">
                            <div class="edit-profile">

                                <button id="btn_edit" class="btn btn-primary btn_avance2">Editar <i class="fa-solid fa-pen"
                                        style="color: #000000;"></i></button>
                                <button id="btn_check" style="display: none;"><i class="fa-regular fa-circle-check"
                                        style="color: #57ae37; font-size: 2rem;"></i></button>
                                <button id="btn_cancelar" style="display: none;"><i
                                        class="fa-sharp fa-regular fa-circle-xmark"
                                        style="color: #a84038; font-size: 2rem;"></i></button>
                            </div>
                            <div>
                                <?php if($errors->has('name')): ?>
                                    <div class="error-message errro-details">
                                        <i class='bx bx-error'></i> <?php echo e($errors->first('name')); ?>

                                    </div>
                                <?php endif; ?>
                                <?php if($errors->has('razas')): ?>
                                    <div class="error-message errro-details">
                                        <i class='bx bx-error'></i> <?php echo e($errors->first('razas')); ?>

                                    </div>
                                <?php endif; ?>
                                <?php if($errors->has('peso')): ?>
                                    <div class="error-message errro-details">
                                        <i class='bx bx-error'></i> <?php echo e($errors->first('peso')); ?>

                                    </div>
                                <?php endif; ?>
                                <?php if($errors->has('year')): ?>
                                    <div class="error-message errro-details">
                                        <i class='bx bx-error'></i> <?php echo e($errors->first('year')); ?>

                                    </div>
                                <?php endif; ?>
                            </div>


                        </div>

                    </div>

                    <div id="advanced-info-container" class="pet-details-pet-info-container advanced-selector disiplayNone">
                        <div class="pet-details-type-info-selector advanced-info-selector">
                            <div class="selector_ofrecemos select_feedback">
                                <p id="vista-vacunas-selector" >Vacunas</p>
                            </div>
                            <div class="selector_ofrecemos">
                                <p id="vista-forma-fisisca-selector">Forma física</p>
                            </div>
                            <div class="selector_ofrecemos">
                                <p id="vista-alimentacion-selector">Alimentación</p>
                            </div>
                        </div>
                        <div class="advanced-info">
                            <div id="vacunas-container">
                                <p>a</p>
                            </div>
                            <div id="forma-fisica-container" class="disiplayNone">
                                <p><?php echo $__env->make('partials.formaFisica', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></p>
                            </div>
                            <div id="alimentacion-container" class="disiplayNone">
                                <p><?php echo $__env->make('partials.pesoComida', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="/js/editPet.js"></script>
    <script src="/js/changePetDetailsView.js"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\jlpacheco\Documents\animalpack\code\resources\views/petDetails/petDetails.blade.php ENDPATH**/ ?>