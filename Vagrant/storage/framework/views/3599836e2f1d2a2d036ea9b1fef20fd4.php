<div class="info-razas-container">
    <script src="https://cdn.lordicon.com/bhenfmcm.js"></script>
    <h1 class="titulo-funcion"> Comida y Peso </h1>
    <?php if($pet->weight >= $raza->pmin && $pet->weight <= $raza->pmax): ?>
        <h1 class="el-peso-es"><span class="peso_span"><i class="fa-solid fa-weight-scale" style="color: white;"></i> Peso: </span><?php echo e($pet->weight); ?> Kg <i class="fa-solid fa-circle-check"
                style="color: #66ff00;"></i></h1>
        <h1 class="title_pesoIdeal"> Peso Ideal: </h1>
        <h1 class="info_ideal"><?php echo e($raza->pmin); ?>Kg <span style="color: white;"><i class="fa-solid fa-angle-right" style="color: white;"></i> <?php echo e($pet->name); ?> <i class="fa-solid fa-angle-left" style="color: white;"></i></span> <?php echo e($raza->pmax); ?>kg</h1>
        <h1 class="title_racion"> Racion Diaria: </h1>
        <div class="info_comida">
            <lord-icon class="peso_icon" src="https://cdn.lordicon.com/wfkxiwtw.json" trigger="hover"
                colors="primary:white,secondary:#f369f9">
            </lord-icon>
            <lord-icon class="peso_icon" src="https://cdn.lordicon.com/xgeyogar.json" trigger="hover"
                colors="primary:white,secondary:#08a88a" >
            </lord-icon>
            <h1 style="color: #f369f9"><?php echo e($raza->comidamed); ?>g</h1>
        
        </div>
    <?php endif; ?>
    <?php if(
        ($pet->weight >= $raza->pmin - $numOperacion && $pet->weight < $raza->pmin) ||
            ($pet->weight <= $raza->pmax + $numOperacion && $pet->weight > $raza->pmax)): ?>
        <h1 class="el-peso-es"><span class="peso_span"><i class="fa-solid fa-weight-scale" style="color: white;"></i> Peso: </span><?php echo e($pet->weight); ?>

            Kg <i class="fa-solid fa-circle-chevron-right" style="color: #ffa200;"></i></h1>
        <h1 class="title_pesoIdeal"> Peso Ideal: </h1>
        <h1 class="info_ideal"><?php echo e($raza->pmin); ?>Kg <span style="color: white;"><i class="fa-solid fa-angle-right" style="color: white;"></i> <?php echo e($pet->name); ?> <i class="fa-solid fa-angle-left" style="color: white;"></i></span> <?php echo e($raza->pmax); ?>kg</h1>
                <h1 class="title_racion"> Racion Diaria: </h1>
                <div class="info_comida">
                        <lord-icon class="peso_icon" src="https://cdn.lordicon.com/wfkxiwtw.json" trigger="hover"
                            colors="primary:white,secondary:#f369f9">
                        </lord-icon>
                        <lord-icon class="peso_icon" src="https://cdn.lordicon.com/xgeyogar.json" trigger="hover"
                            colors="primary:white,secondary:#08a88a" >
                        </lord-icon>
                        
                        <h1 style="color: #f369f9"><?php echo e($raza->comidamed); ?>g</h1>
            
                    </div>
    <?php endif; ?>
    <?php if($pet->weight < $raza->pmin - $numOperacion): ?>
        <h1 class="el-peso-es"><span class="peso_span"><i class="fa-solid fa-weight-scale" style="color: white;"></i> Peso: </span><?php echo e($pet->weight); ?> Kg <i class="fa-solid fa-circle-chevron-down"
                style="color: #ff0000;"></i>
        </h1>
        <h1 class="title_pesoIdeal"> Peso Ideal: </h1>
        <h1 class="info_ideal"><?php echo e($raza->pmin); ?>Kg <span style="color: white;"><i class="fa-solid fa-angle-right" style="color: white;"></i> <?php echo e($pet->name); ?> <i class="fa-solid fa-angle-left" style="color: white;"></i></span> <?php echo e($raza->pmax); ?>kg
        </h1>
        <h1 class="title_racion"> Racion Diaria: </h1>
        <div class="info_comida">
            <lord-icon class="peso_icon" src="https://cdn.lordicon.com/wfkxiwtw.json" trigger="hover"
                colors="primary:white,secondary:#f369f9">
            </lord-icon>
            <lord-icon class="peso_icon" src="https://cdn.lordicon.com/xgeyogar.json" trigger="hover"
                colors="primary:white,secondary:#08a88a" >
            </lord-icon>

            <h1 style="color: #f369f9"><?php echo e($raza->comidamax); ?>g</h1>

        </div>
    <?php endif; ?>
    <?php if($pet->weight > $raza->pmax + $numOperacion): ?>
        <h1 class="el-peso-es"><span class="peso_span"><i class="fa-solid fa-weight-scale" style="color: white;"></i> Peso: </span><?php echo e($pet->weight); ?> Kg <i class="fa-solid fa-circle-chevron-up"
                style="color: #ff0000;"></i>
        </h1>
        <h1 class="title_pesoIdeal"> Peso Ideal: </h1>
        <h1 class="info_ideal"><?php echo e($raza->pmin); ?>Kg <span style="color: white;"><i class="fa-solid fa-angle-right" style="color: white;"></i> <?php echo e($pet->name); ?> <i class="fa-solid fa-angle-left" style="color: white;"></i></span> <?php echo e($raza->pmax); ?>kg
        </h1>
        <h1 class="title_racion"> Racion Diaria: </h1>
        <div class="info_comida">
                <lord-icon class="peso_icon" src="https://cdn.lordicon.com/wfkxiwtw.json" trigger="hover"
                    colors="primary:white,secondary:#f369f9">
                </lord-icon>
                <lord-icon class="peso_icon" src="https://cdn.lordicon.com/xgeyogar.json" trigger="hover"
                    colors="primary:white,secondary:#08a88a" >
                </lord-icon>
    
                <h1 style="color: #f369f9"><?php echo e($raza->comidamin); ?>g</h1>
    
            </div>
    <?php endif; ?>


</div>
<?php /**PATH C:\Users\jlpacheco\Documents\animalpack\code\resources\views/partials/pesoComida.blade.php ENDPATH**/ ?>