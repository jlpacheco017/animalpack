
<?php $__env->startSection('title', 'AnimalPack'); ?>
<?php $__env->startSection('content'); ?>
    <h1>Planes de Seguro para Perros</h1>
    <div class="container_planes">

        <?php echo csrf_field(); ?>
        <div class="proceso-pago-general" style="display: none">
           <div class="proceso-pago">
            <h1>Procesando pago...</h1>
            <script src="https://cdn.lordicon.com/bhenfmcm.js"></script>
            <lord-icon src="https://cdn.lordicon.com/ujmqspux.json" trigger="hover"
                colors="primary:#121331,secondary:#ebe6ef,tertiary:#2ca58d" style="width:250px;height:250px">
            </lord-icon>
        </div> 
        </div>
        
        <div class="plan">
            <div>

                <h2 class="plan-title">AnimalPack-Basic</h2>
                <div class="plan-coverage">Cobertura: Accidentes y emergencias veterinarias</div>
            </div>
            <div class="plan-details">
                <ul>
                    <li> <i class="fa-solid fa-circle-check"></i> Cobertura de hasta $1,000 por año</li>
                    <li> <i class="fa-solid fa-circle-check"></i> Servicios incluidos: Consultas de emergencia,
                        radiografías
                        y análisis de laboratorio básicos
                    </li>
                    <li> <i class="fa-solid fa-circle-check"></i> Acceso a una línea telefónica de consulta veterinaria
                        las
                        24 horas del día</li>
                    <li style="text-decoration:line-through;">Descuentos en servicios preventivos, programas de
                        vacunación y
                        esterilización</li>
                    <li style="text-decoration:line-through;">Reembolso de gastos de viaje en caso de emergencia médica
                        del
                        dueño</li>
                </ul>
            </div>

            <div class="price_container">
                <div class="plan-price">10€/Mes</div>

                <?php if(Auth::user()->subscripcion == 'Basic'): ?>
                    <form action="<?php echo e(route('animal.cancelSub')); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="plan" value="premium">
                        <button type="submit" class="btn_Adquirido" dissabled><b>CANCELAR</b></button>
                    </form>
                <?php else: ?>
                    <form action="<?php echo e(route('animal.newSub')); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="plan" value="basic">
                        <button type="submit" class="btn-subscribe">SUBSCRIBIRSE</button>
                    </form>
                <?php endif; ?>

            </div>
        </div>

        <div class="plan plan-intermedio">
            <div class="plan-popular"><i class="fa-solid fa-fire" style="color: #0957dc;"></i> El mas popular <i
                    class="fa-solid fa-fire" style="color: #0957dc;"></i></div>
            <h2 class="plan-title">AnimalPack-Plus</h2>
            <div class="plan-coverage">Cobertura: Accidentes, enfermedades y emergencias veterinarias</div>
            <div class="plan-details">
                <ul>
                    <li> <i class="fa-solid fa-circle-check"></i> Cobertura de hasta $5,000 por año</li>
                    <li> <i class="fa-solid fa-circle-check"></i> Servicios incluidos: Consultas de emergencia,
                        radiografías, análisis de laboratorio,
                        medicamentos y tratamientos</li>
                    <li> <i class="fa-solid fa-circle-check"></i> Acceso a una línea telefónica de consulta veterinaria
                        las
                        24 horas del día</li>
                    <li> <i class="fa-solid fa-circle-check"></i> Descuentos en servicios preventivos, programas de
                        vacunación y esterilización</li>
                    <li style="text-decoration:line-through;">Reembolso de gastos de viaje en caso de emergencia médica
                        del
                        dueño</li>
                </ul>
            </div>
            <div class="price_container">
                <div class="plan-price">20€/Mes</div>
                <?php if(Auth::user()->subscripcion == 'Plus'): ?>
                    <form action="<?php echo e(route('animal.cancelSub')); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="plan" value="premium">
                        <button type="submit" class="btn_Adquirido" dissabled><b>CANCELAR</b></button>
                    </form>
                <?php else: ?>
                    <form action="<?php echo e(route('animal.newSub')); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="plan" value="plus">
                        <button type="submit" class="btn-subscribe">SUBSCRIBIRSE</button>
                    </form>
                <?php endif; ?>
            </div>
        </div>

        <div class="plan">
            <h2 class="plan-title">AnimalPack-Premium</h2>
            <div class="plan-coverage">Cobertura: Accidentes, enfermedades, emergencias veterinarias y servicios
                preventivos</div>
            <div class="plan-details">
                <ul>
                    <li> <i class="fa-solid fa-circle-check"></i> Cobertura de hasta $10,000 por año</li>
                    <li> <i class="fa-solid fa-circle-check"></i> Servicios incluidos: Consultas de emergencia,
                        radiografías, análisis de laboratorio,
                        medicamentos, tratamientos y cirugías</li>
                    <li> <i class="fa-solid fa-circle-check"></i> Acceso a una línea telefónica de consulta veterinaria
                        las
                        24 horas del día</li>
                    <li> <i class="fa-solid fa-circle-check"></i> Descuentos en servicios preventivos, programas de
                        vacunación y esterilización</li>
                    <li> <i class="fa-solid fa-circle-check"></i> Reembolso de gastos de viaje en caso de emergencia
                        médica
                        del dueño</li>
                </ul>
            </div>
            <div class="price_container">
                <div class="plan-price">30€/Mes</div>

                <?php if(Auth::user()->subscripcion == 'Premium'): ?>
                    <form action="<?php echo e(route('animal.cancelSub')); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="plan" value="premium">
                        <button type="submit" class="btn_Adquirido" dissabled><b>CANCELAR</b></button>
                    </form>
                <?php else: ?>
                    <form action="<?php echo e(route('animal.newSub')); ?>" method="post">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="plan" value="premium">
                        <button type="submit" class="btn-subscribe">SUBSCRIBIRSE</button>
                    </form>
                <?php endif; ?>

            </div>
        </div>
        </form>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
    <script src="/js/plan.js"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\jlpacheco\Documents\animalpack\code\resources\views/planes/planes.blade.php ENDPATH**/ ?>