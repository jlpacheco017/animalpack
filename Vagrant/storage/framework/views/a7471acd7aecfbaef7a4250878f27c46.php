
<?php $__env->startSection('title', 'AnimalPack - HOME'); ?>
<?php $__env->startSection('content'); ?>
    <div class="index_content">
        <div class="titleContainer">
            <h2>Tus mascotas</h2>
            <a href="<?php echo e(route('addPet.showAddPet')); ?>"><b><i class="fa-solid fa-plus"></i> Añadir Animal</b></a>
        </div>
        <div class="products-section">
            <?php $__errorArgs = ['error'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="error-message">
                    <i class='bx bx-error'></i> <?php echo e($message); ?>

                </div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            <?php $__currentLoopData = $pet; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $pet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <a href="<?php echo e(route('petDetail.showPetDetail', ['id' => $pet->id])); ?>" class="container">
                    <div class="item_img">
                        <img src="<?php echo e(asset('img/pets/' . $pet->imagen)); ?>" class="img_mascotas"
                            alt="imagen de un <?php echo e($pet->breed); ?>" srcset="">
                    </div>
                    <div>
                        <div class="petNameContainer">
                            <p><b>Nombre: </b><?php echo e($pet->name); ?>

                            </p>
                        </div>
                        <div class="petNameContainer">
                            <p><b>Raza: </b><?php echo e($pet->breed); ?>

                            </p>
                        </div>
                        <?php
                        $cumpleaños = \Carbon\Carbon::parse($pet->age);
                        $fechaDeHoy = \Carbon\Carbon::now();
                        $años = $cumpleaños->diffInYears($fechaDeHoy);
                        ?>
                        <div class="petNameContainer">
                            <p><b>Edad: </b><?php echo e($años); ?> Años</p>
                        </div>

                        <div class="petNameContainer">
                            <p><b>Peso: </b><?php echo e($pet->weight); ?> Kg
                            </p>
                        </div>
                    </div>
                </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\jlpacheco\Documents\animalpack\code\resources\views/home/home.blade.php ENDPATH**/ ?>