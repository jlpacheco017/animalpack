
<?php $__env->startSection('title', 'AnimalPack - Nuevo Animal'); ?>
<?php $__env->startSection('content'); ?>
    <div class="add-pet-container">
        <form action="" method="post" id="form-add-animal" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <h2>Añade tu mascota</h2>

            <div class="login_inputs">
                <label for="nombreMascota">Nombre Mascota</label>
                <input class="login_form" id="nombreMascota" type="text" name="name" placeholder="Nombre" />
            </div>
            <div class="login_inputs">
                <label for="nacimientoMascota">Fecha de nacimiento:</label>
                <input class="login_form" id="nacimientoMascotaInput" type="date" value="" name="age" />
            </div>

            <div class="login_inputs">
                <label for="razaMascota">Raza:</label>
                <select name="breed" id="razaMascota">
                    <option class="login_form" value="" disabled selected>Selecciona una raza...</option>
                    <?php $__currentLoopData = $razas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $raza): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($raza->raza); ?>"><?php echo e($raza->raza); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>

            <div class="login_inputs">
                <label for="pesoMascota">Peso:</label>
                <input class="login_form" name="weight" id="pesoMascota"type="text" onkeypress="validate(event)"
                    placeholder="20 Kg" />
            </div>
            <div class="login_inputs">
                <label for="pesoMascota">Peso:</label>
                <input class="login_form" type="file" name="fotoMascota" class="img-select"
                    accept="image/png, image/gif, image/jpeg, image/jpg" />
            </div>
            <?php if($errors->has('name')): ?>
                <div class="error-message">
                    <i class='bx bx-error'></i> <?php echo e($errors->first('name')); ?>

                </div>
            <?php endif; ?>
            <?php if($errors->has('breed')): ?>
                <div class="error-message">
                    <i class='bx bx-error'></i> <?php echo e($errors->first('breed')); ?>

                </div>
            <?php endif; ?>
            <?php if($errors->has('weight')): ?>
                <div class="error-message">
                    <i class='bx bx-error'></i> <?php echo e($errors->first('weight')); ?>

                </div>
            <?php endif; ?>
            <?php if($errors->has('age')): ?>
                <div class="error-message">
                    <i class='bx bx-error'></i> <?php echo e($errors->first('age')); ?>

                </div>
            <?php endif; ?>
            <?php if($errors->has('fotoMascota')): ?>
                <div class="error-message">
                    <i class='bx bx-error'></i> <?php echo e($errors->first('fotoMascota')); ?>

                </div>
            <?php endif; ?>

            <button type="submit" class="btn btn-primary btn_avance2" value="Crear"><b>Crear</b></button>
        </form>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\jlpacheco\Documents\animalpack\code\resources\views/addPet/addPet.blade.php ENDPATH**/ ?>