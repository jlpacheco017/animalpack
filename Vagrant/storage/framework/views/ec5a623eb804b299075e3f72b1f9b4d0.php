
<?php $__env->startSection('title', 'AnimalPack - Login'); ?>
<?php $__env->startSection('content'); ?>
<div class="login">
        <div class="login_superior">
            <img class="foto_login" src="<?php echo e(asset('img/login_image.png')); ?>" srcset="">
        </div>
        <form method="post">
            <?php echo csrf_field(); ?>
            <h1 class="titulo_login">INICIAR SESION</h1>
            
            <div class="login_inputs">
                <i id="login_icon" class="fa-solid fa-user"></i>
                <input class="login_form" type="text" id="email" name="email" placeholder="Usuario" required>

            </div>
            <div class="login_inputs">
                <i id="login_icon" class="fa-solid fa-lock"></i>
                <input class="login_form" type="password" id="password" name="password" placeholder="Contraseña"
                    required>
            </div>
            <?php $__errorArgs = ['login'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                <div class="error-message">
                    <i class='bx bx-error'></i> <?php echo e($message); ?>

                </div>
            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
            <?php if(session('succes')): ?>
                <div class="succes-message">
                    <i class='bx bx-error'></i> <?php echo e(session('succes')); ?>

                </div>
            <?php endif; ?>
            <button type="submit" class="btn btn-primary btn_avance2">Login</button>
            <a href="<?php echo e(route('auth.showRegister')); ?>" id="btn_volver" class="btn_volver2"><b>REGISTRARSE</b></a>
    </form>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\jlpacheco\Documents\animalpack\code\resources\views/auth/login.blade.php ENDPATH**/ ?>