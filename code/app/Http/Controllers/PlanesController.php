<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlanesController extends Controller
{
    public function showPlanes()
    {
            return view('planes.planes');
        
    }
    public function cancelSub()
    {
            $user = Auth::User();
            $user->subscripcion = "Free";
            $user->save();

            return back();
        
    }
    public function newSub(Request $request)
    {
            sleep(5);
            $user = Auth::User();
            $user->subscripcion = $request->plan;
            $user->save();

            return back();
        
    }
}
