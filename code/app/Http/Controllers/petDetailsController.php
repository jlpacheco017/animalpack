<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Pets;
use App\Models\Razas;
use App\Models\Vaccine;
use Nette\Utils\DateTime;
use Illuminate\Http\Request;
use App\Models\PhysicalActivity;
use Illuminate\Support\Facades\Log;
use App\Models\PetCenterVaccinesPet;
use App\Models\PetCenterVaccine;
use Illuminate\Support\Facades\Auth;

class petDetailsController extends Controller
{
    public function showPetDetail($id)
    {
        if (Pets::findOrFail($id)->user_id == auth()->user()->id) {
            $animal = Pets::findOrFail($id);
            $razaInfo = Razas::where('raza', $animal->breed)->first();
            $numLimite = 0;

            if ($razaInfo->pmin >= 15) {
                $numLimite = 5;
            }
            if ($razaInfo->pmin < 15 && $razaInfo->pmin >= 10) {
                $numLimite = 3;
            }
            if ($razaInfo->pmin < 10 && $razaInfo->pmin >= 5) {
                $numLimite = 2;
            }
            if ($razaInfo->pmin <= 5) {
                $numLimite = 0.5;
            }
            $physicalActivity = PhysicalActivity::physycalActvityFromPet($id);

            $time = new DateTime("00:00");

            foreach ($physicalActivity as $pa) {
                $time->add(date_create($pa->started_at)->diff(date_create($pa->finished_at)));
            }

            $recomendedTime = PhysicalActivity::recomendedPhysicalActivity($id);
            $razas = Razas::all();

            return view('petDetails.petDetails', [
                'pet' => $animal,
                'raza' => $razaInfo,
                'numOperacion' => $numLimite,
                'physicalActivity' => $physicalActivity,
                'time' => $time->format('H:i:s'),
                "recomendedTime" => $recomendedTime,
                'razas' => $razas,
                'nextVaccine'   => $this->nextVaccines($id),
                'vaccineHistory' => $this->vaccioneHistory($id)
            ]);
        } else {
            return redirect()->route('home.home')->withErrors(['error' => 'No tienes acceso al animal seleccionado o no esta disponible su información']);
        }
    }
    public function editPet(Request $request)
    {
        $user = Auth::user();
        $pet = Pets::where('user_id', $user->id)->where('id', $request->id_pet)->first();
        if ($pet) {
            $request->validate(
                [
                    'name' => 'required',
                    'razas' => 'required',
                    'peso' => 'required',
                    'year' => [
                        'required',
                        'date',
                        'before_or_equal:today',
                    ],
                ],
                [
                    'name.required' => 'El nombre es obligatorio.',
                    'razas.required' => 'La raza es obligatoria.',
                    'peso.required' => 'El peso es obligatorio.',
                    'year.required' => 'El campo fecha es obligatorio.',
                    'year.date' => 'El campo fecha debe ser una fecha válida.',
                    'year.before_or_equal' => 'La fecha no puede ser superior a hoy.',
                ]
            );
            $age = Carbon::createFromFormat('Y-m-d', $request->year);
            $diffYears = $age->diffInYears(Carbon::now());
            if ($diffYears > 20) {
                return back()->withErrors(['year' => 'La edad no puede ser mayor a 20 años.']);
            }
            $pet->name = $request->name;
            $pet->age = $request->year;
            $pet->breed = $request->razas;
            $pet->weight = $request->peso;
            $pet->save();
        }
    }

    public function activatePhysicalActivity($id)
    {
        $currentPhysicalActivity = PhysicalActivity::checkForActivePhysicalActivity($id);


        if ($currentPhysicalActivity != null) {
            PhysicalActivity::endPhysicalActivity($currentPhysicalActivity);
        } else {
            PhysicalActivity::startPhysicalActivity($id);
        }

        return view('petDetails.petDetails');
    }

    public static function vaccioneHistory($petId)
    {
        return PetCenterVaccinesPet::getInfoByPetId($petId);
    }

    public function nextVaccines($id)
    {
        // Obtenemos las semanas del perro
        $weekOfPet = Pets::getWeekOfPet($id);
        // Ponemos en variable la fecha de nacimiento del perro
        $pet = Pets::find($id);
        $age = $pet->age;

        // // Calcylamos las siguientes vacunas
        $vaccine = Vaccine::getNextVaccine($weekOfPet, $age, $pet);

        $vaccineWithCenter = [];


        foreach ($vaccine as $key) {
            if ($key->is_annual == 1) {

                $pet_center_name = PetCenterVaccinesPet::where('pet_id', $id)
                    ->where(function ($query) {
                        $query->whereNull('applied')
                            ->orWhere('applied', 0);
                    })
                    ->join('pet_center_vaccines', 'pet_center_vaccines.id', '=', 'pet_center_vaccines_pets.pet_center_vaccine_id')
                    ->join('pet_centers', 'pet_centers.id', '=', 'pet_center_vaccines.pet_center_id')
                    ->select('pet_centers.name')
                    ->first();

                if (!is_null($pet_center_name)) {
                    $key->center_name = $pet_center_name->name;
                } 

                $vaccineWithCenter[] = $key;
            } else {
                $petCenterVaccinePet = PetCenterVaccinesPet::with('pet_center_vaccine.pet_center', 'pet_center_vaccine.vaccine')
                    ->whereHas('pet_center_vaccine.vaccine', function ($query) use ($key) {
                        $query->where('id', $key->id);
                    })
                    ->first();

                $vaccineWithCenterItem = clone $key;

                if ($petCenterVaccinePet) {
                    $petCenterName = $petCenterVaccinePet->pet_center_vaccine->pet_center->name;

                    $vaccineWithCenterItem->center_name = $petCenterName;
                } else {
                    $vaccineWithCenterItem->center_name = null;
                }

                $vaccineWithCenter[] = $vaccineWithCenterItem;
            }
        }
        return $vaccineWithCenter;
    }

    public function showCenter($id)
    {
        $pet = Pets::findOrFail($id);

        return view('petDetails.centerDetails.blade', [
            'pet'           => $pet,
            'vaccineHistory' => $this->vaccioneHistory($id)
        ]);
    }

    public function showCentersVacine($id, $id_vaccine)
    {
        if (Pets::findOrFail($id)->user_id == auth()->user()->id) {
            $animal = Pets::findOrFail($id);
        }
        $centers = PetCenterVaccine::centersWithThisVaccine($id_vaccine);

        return view('petDetails.centerDetails', [
            'pet' => $animal,
            'centers' => $centers,
        ]);
    }

    public function SelectedCenter(Request $request)
    {
        $validated = $request->validate([
            'idCenterVaccine' => 'required|integer',
            'idPet' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        $record = PetCenterVaccinesPet::create([
            'pet_center_vaccine_id' => $validated['idCenterVaccine'],
            'mandatory' => true,
            'applied' => false,
            'pet_id' => $validated['idPet'],
            'price' => $validated['price'],
        ]);

        return $this->showPetDetail($request->idPet)->with('success', 'Record has been added successfully.');
    }
}
