<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PetCenter extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'address',
        'phone',
        'emergency_phone',
        'zip_code',
        'city',
        'country',
        'contact_name',
        'email',
        'website'
    ];

    public function vaccine()
    {
        return $this->belongsToMany(Vaccine::class);
    }

    public function vaccines()
    {
        return $this->belongsToMany(Vaccine::class, 'pet_center_vaccines', 'pet_center_id', 'vaccine_id')
            ->withPivot('date_applied', 'mandatory', 'applied');
    }

    public function petVaccineRelations()
    {
        return $this->belongsToMany(Pet::class, 'pet_center_vaccines_pets')
            ->withPivot('vaccine_id', 'price')
            ->as('relation');
    }

    public function petCenterVaccines()
    {
        return $this->hasMany(PetCenterVaccine::class, 'pet_center_id', 'id');
    }

    public function pet_center_vaccines()
    {
        return $this->belongsToMany(Vaccine::class, 'pet_center_vaccines')->withPivot('atributo_adicional');
    }
}
