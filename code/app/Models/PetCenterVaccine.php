<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PetCenterVaccine extends Model
{
    protected $table = 'pet_center_vaccines';

    public function pet_center()
    {
        return $this->belongsTo(PetCenter::class);
    }

    public function petCenter()
    {
        return $this->belongsTo(PetCenter::class, 'pet_center_id', 'id');
    }

    public function vaccine()
    {
        return $this->belongsTo(Vaccine::class);
    }

    public function pet_center_vaccines_pets()
    {
        return $this->hasMany(PetCenterVaccinePet::class);
    }

    public static function centersWithThisVaccine($vaccineId) {
        return self::with('petCenter')->where('vaccine_id', $vaccineId)->get();
    }
    
}
