<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PetCenterVaccinesPet extends Model
{
    use HasFactory;

    protected $fillable = ['date_applied', 'pet_center_vaccine_id', 'mandatory', 'applied', 'pet_id', 'price'];

    public function pet() {
        return $this->belongsTo(Pets::class);
    }

    public function pet_center_vaccine() {
        return $this->belongsTo(PetCenterVaccine::class, 'pet_center_vaccine_id');
    }

    public static function getInfoByPetId($petId) {
        $today = Carbon::today();

        return self::with(['pet_center_vaccine.pet_center', 'pet_center_vaccine.vaccine'])
            ->where('pet_id', $petId)
            ->whereDate('date_applied', '<=', $today)
            ->get();
    }
}
