<?php

namespace App\Models;

use Ramsey\Uuid\Type\Integer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pets extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'age', 'breed', 'weight', 'user_id'];


    static function getPetsFromUserId($id)
    {
        return Pets::all()->where("user_id", $id);
    }

    public function user()
    {
        return $this->belongsTo(Pets::class);
    }

    public function pet_center_vaccines_pets()
    {
        return $this->hasMany(PetCenterVaccinePet::class);
    }

    static function getWeekOfPet($id)
    {
        $pet = Pets::selectRaw('TIMESTAMPDIFF(WEEK, age, NOW()) AS age_in_weeks')->where('id', $id)->first();
        return $pet->age_in_weeks;
    }
}
