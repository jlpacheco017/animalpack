<?php

namespace App\Models;

use DateTimeZone;
use App\Models\Pets;
use App\Models\Razas;
use App\Models\PhysicalActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PhysicalActivity extends Model
{
    use HasFactory;
    protected $fillable =
    [
        'started_at',
        'finished_at',
        'pet_id',
        'active'
    ];

    public static function physycalActvityFromPet($id)
    {
        return PhysicalActivity::where("pet_id", $id)->where("active", false)->orderBy('created_at', 'desc')->get();
    }

    public static function recomendedPhysicalActivity($id)
    {
        $pet = Pets::all()->where("id", $id)->first();
        $breed = Razas::all()->where("raza", $pet->breed)->first();
        return $breed->recomendedTime;
    }

    public static function checkForActivePhysicalActivity($id)
    {
        $currentPet = Pets::all()->where("id", $id)->first();
        $currentPhysicalActivity = PhysicalActivity::all()->where("pet_id", $currentPet->id)->where("active", true)->first();
        return $currentPhysicalActivity;
    }

    public static function endPhysicalActivity($currentPhysicalActivity)
    {
        $date = date_create();

        date_timezone_set($date, new DateTimeZone('Europe/Madrid'));

        $currentPhysicalActivity->finished_at = $date;
        $currentPhysicalActivity->active = false;
        $currentPhysicalActivity->save();
    }

    public static function startPhysicalActivity($id)
    {
        $date = date_create();

        date_timezone_set($date, new DateTimeZone('Europe/Madrid'));

        $newPhysicalActivity = new PhysicalActivity([
            'started_at' => $date,
            'active' => true,
            'pet_id' => $id
        ]);

        $newPhysicalActivity->save();
    }
}
