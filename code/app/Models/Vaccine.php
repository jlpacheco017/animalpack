<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Vaccine extends Model
{
    use HasFactory;

    protected $table = 'vaccines';


    protected $fillable = [
        'type_of_vaccine',
        'weeks_administration',
        'description',
        'company',
        'is_annual',
        'is_mandatory',
    ];

    public function pet_centers()
    {
        return $this->belongsToMany(PetCenter::class, 'pet_center_vaccines')->withPivot('price');
    }

    public function petCenterPetRelations()
    {
        return $this->belongsToMany(Pets::class, 'pet_center_vaccines_pets')
            ->withPivot('pet_center_id', 'price')
            ->as('relation');
    }

    public static function getNextVaccine($weeks, $age, $pet)
    {
        $age = Carbon::parse($age);
        $diffInDays = $age->diffInDays(Carbon::now());

        if ($diffInDays >= 365) {
            // Código para perros mayores a un año
            $birthdate = Carbon::parse($age);
            $today = Carbon::now();
            $thisYearBirthday = $birthdate->copy()->year($today->year);
            if ($today->greaterThanOrEqualTo($thisYearBirthday)) {
                $nextBirthday = $thisYearBirthday->addYear();
            } else {
                $nextBirthday = $thisYearBirthday;
            }

            $vaccines = self::where('is_annual', true)
                ->orderBy('weeks_administration')
                ->get();

            $appliedVaccines = PetCenterVaccinesPet::where('pet_id', $pet->id)
                ->where('applied', true)
                ->pluck('pet_center_vaccine_id');

            $appliedVaccineIds = PetCenterVaccine::whereIn('id', $appliedVaccines)
                ->pluck('vaccine_id');
            $appliedVaccineIds = $appliedVaccineIds->toArray();


            foreach ($vaccines as $key => $vaccine) {
                if (in_array($vaccine->id, $appliedVaccineIds)) {
                    unset($vaccines[$key]);
                }
            }

            $vaccines = collect($vaccines->values());


            foreach ($vaccines as $vaccine) {
                // Calcular la fecha de la próxima dosis basándose en el próximo cumpleaños del perro
                $nextDoseDate = $nextBirthday->copy(); // No agregar las semanas de administración
                $vaccine->scheduled_date = $nextDoseDate->format('d/m/Y');
            }
            return $vaccines;
        } else {
            // La edad del perro es menor a un año
            $oneYearFromNow = Carbon::now()->addYear();

            $vaccines = self::where('weeks_administration', '>', $weeks)
                ->where('weeks_administration', '<', $oneYearFromNow)
                ->orderBy('weeks_administration')
                ->get();

            $appliedVaccines = PetCenterVaccinesPet::where('pet_id', $pet->id)
                ->where('applied', true)
                ->pluck('pet_center_vaccine_id');


            $appliedVaccineIds = PetCenterVaccine::whereIn('id', $appliedVaccines)
                ->pluck('vaccine_id');
            $appliedVaccineIds = $appliedVaccineIds->toArray();

            foreach ($vaccines as $key => $vaccine) {
                if (in_array($vaccine->id, $appliedVaccineIds)) {
                    unset($vaccines[$key]);
                }
            }

            $vaccines = collect($vaccines->values());

            foreach ($vaccines as $vaccine) {
                $date = Carbon::parse($age)->addWeeks($vaccine->weeks_administration);
                $vaccine->scheduled_date = $date->format('d/m/Y');
            }

            return $vaccines;
        }
    }
}
