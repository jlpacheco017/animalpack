<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


class PetCenterFactory extends Factory
{
    public function definition(): array
    {
        $city_options = [
            'Terrassa' => ['08221', '08222', '08223', '08224', '08225', '08226', '08227', '08228'],
            'Sabadell' => ['08192', '08201', '08202', '08203', '08204', '08205', '08206', '08207', '08208', '08805'],
            'Barcelona' => ['08001', '08005', '08007', '08009', '08011', '08014', '08019', '08021', '08025', '08028'],
            'Cerdanyola' => ['08193', '08214', '08290', '08340'],
            'Rubí' => ['08101', '08191']
        ];

        $city = $this->faker->randomElement(array_keys($city_options));

        return [
            'name' => fake()->company(),
            'address' => fake()->address(),
            'phone' => fake()->phoneNumber(),
            'emergency_phone' => fake()->phoneNumber(),
            'postal_code' => fake()->randomElement($city_options[$city]),
            'city' => $city,
            'country' => 'España',
            'contact_person_name' => fake()->name(),
            'contact_person_email' => fake()->email(),
            'website' => fake()->url()
        ];
    }
}
