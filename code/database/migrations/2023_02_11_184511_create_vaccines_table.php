<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('vaccines', function (Blueprint $table) {
            $table->id();
            $table->string('type_of_vaccine');
            $table->integer('weeks_administration');
            $table->string('description')->nullable();
            $table->string('company');
            $table->boolean('is_annual');
            $table->boolean('is_mandatory');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('vaccine');
    }
};
