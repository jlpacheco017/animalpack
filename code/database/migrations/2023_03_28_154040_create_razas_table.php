<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('razas', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('raza');
            $table->integer('pmin');
            $table->integer('pmax');
            $table->integer('comidamin');
            $table->integer('comidamed');
            $table->integer('comidamax');
            $table->integer('recomendedTime')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('razas');
    }
};
