<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('pet_center_vaccines', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pet_center_id');
            $table->unsignedBigInteger('vaccine_id');
            $table->decimal('price', 8, 2)->nullable();
            $table->timestamps();
            $table->foreign('pet_center_id')->references('id')->on('pet_centers');
            $table->foreign('vaccine_id')->references('id')->on('vaccines');
        });
    }
    public function down(): void
    {
        Schema::dropIfExists('pet_center_vaccines');
    }
};
