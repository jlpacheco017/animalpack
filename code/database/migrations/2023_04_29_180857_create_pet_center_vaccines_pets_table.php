<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('pet_center_vaccines_pets', function (Blueprint $table) {
            $table->id();
            $table->date('date_applied')->nullable();
            $table->unsignedBigInteger('pet_center_vaccine_id');
            $table->boolean('mandatory');
            $table->boolean('applied');
            $table->unsignedBigInteger('pet_id');
            $table->decimal('price', 8, 2)->nullable();
            $table->timestamps();

            // Foreign key constraint
            $table->foreign('pet_center_vaccine_id')
                ->references('id')
                ->on('pet_center_vaccines')
                ->onDelete('cascade');

            $table->foreign('pet_id')
                ->references('id')
                ->on('pets')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('pet_center_vaccine_pets', function (Blueprint $table) {
            $table->dropForeign(['pet_center_vaccine_id']);
            $table->dropForeign(['pet_id']);
        });

        Schema::dropIfExists('pet_center_vaccines_pets');
    }
};
