<?php

namespace Database\Seeders;

use App\Models\PetCenter;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PetCenterSeeder extends Seeder
{
    public function run(): void
    {
        PetCenter::factory(50)->create();
    }
}
