<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RazasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $razas = [

            ['raza' => 'Labrador Retriever', 'pmin' => 25, 'pmax' => 36, 'comidamin' => 360, 'comidamed' => 480, 'comidamax' => 600, 'recomendedTime' => 60],
            ['raza' => 'Pastor Alemán', 'pmin' => 30, 'pmax' => 40, 'comidamin' => 480, 'comidamed' => 540, 'comidamax' => 600, 'recomendedTime' => 60],
            ['raza' => 'Bulldog Inglés', 'pmin' => 18, 'pmax' => 23, 'comidamin' => 250, 'comidamed' => 325, 'comidamax' => 400, 'recomendedTime' => 30],
            ['raza' => 'Golden Retriever', 'pmin' => 25, 'pmax' => 34, 'comidamin' => 360, 'comidamed' => 435, 'comidamax' => 510, 'recomendedTime' => 60],
            ['raza' => 'Beagle', 'pmin' => 9, 'pmax' => 16, 'comidamin' => 135, 'comidamed' => 185, 'comidamax' => 240, 'recomendedTime' => 30],
            ['raza' => 'Poodle', 'pmin' => 2, 'pmax' => 32, 'comidamin' => 125, 'comidamed' => 140, 'comidamax' => 150, 'recomendedTime' => 30],
            ['raza' => 'Yorkshire Terrier', 'pmin' => 1, 'pmax' => 3, 'comidamin' => 20, 'comidamed' => 40, 'comidamax' => 60, 'recomendedTime' => 20],
            ['raza' => 'Boxer', 'pmin' => 25, 'pmax' => 32, 'comidamin' => 360, 'comidamed' => 410, 'comidamax' => 480, 'recomendedTime' => 60],
            ['raza' => 'Chihuahua', 'pmin' => 1, 'pmax' => 3, 'comidamin' => 20, 'comidamed' => 40, 'comidamax' => 60, 'recomendedTime' => 20],
            ['raza' => 'Dachshund', 'pmin' => 4, 'pmax' => 15, 'comidamin' => 60, 'comidamed' => 150, 'comidamax' => 225, 'recomendedTime' => 30],
            ['raza' => 'Rottweiler', 'pmin' => 50, 'pmax' => 60, 'comidamin' => 750, 'comidamed' => 850, 'comidamax' => 900, 'recomendedTime' => 60],
            ['raza' => 'Siberian Husky', 'pmin' => 16, 'pmax' => 28, 'comidamin' => 240, 'comidamed' => 350, 'comidamax' => 420, 'recomendedTime' => 60],
            ['raza' => 'Doberman Pinscher', 'pmin' => 27, 'pmax' => 45, 'comidamin' => 405, 'comidamed' => 520, 'comidamax' => 675, 'recomendedTime' => 60],
            ['raza' => 'Shih Tzu', 'pmin' => 4, 'pmax' => 7, 'comidamin' => 60, 'comidamed' => 80, 'comidamax' => 105, 'recomendedTime' => 30],
            ['raza' => 'Pomeranian', 'pmin' => 1.4, 'pmax' => 3.2, 'comidamin' => 21, 'comidamed' => 45, 'comidamax' => 64, 'recomendedTime' => 30],
            ['raza' => 'Bichon Frise', 'pmin' => 3, 'pmax' => 5, 'comidamin' => 45, 'comidamed' => 60, 'comidamax' => 75, 'recomendedTime' => 30],
            ['raza' => 'Great Dane', 'pmin' => 54, 'pmax' => 90, 'comidamin' => 810, 'comidamed' => 1000, 'comidamax' => 1350, 'recomendedTime' => 60],
            ['raza' => 'Border Collie', 'pmin' => 12, 'pmax' => 20, 'comidamin' => 180, 'comidamed' => 240, 'comidamax' => 300, 'recomendedTime' => 60],
            ['raza' => 'Cavalier King Charles Spaniel', 'pmin' => 5, 'pmax' => 8, 'comidamin' => 70, 'comidamed' => 85, 'comidamax' => 100, 'recomendedTime' => 30],
            ['raza' => 'Boston Terrier', 'pmin' => 5, 'pmax' => 11, 'comidamin' => 100, 'comidamed' => 150, 'comidamax' => 200, 'recomendedTime' => 30],
            ['raza' => 'Bernese Mountain Dog', 'pmin' => 38, 'pmax' => 50, 'comidamin' => 600, 'comidamed' => 900, 'comidamax' => 1200, 'recomendedTime' => 60],
            ['raza' => 'Australian Shepherd', 'pmin' => 16, 'pmax' => 32, 'comidamin' => 300, 'comidamed' => 450, 'comidamax' => 600, 'recomendedTime' => 60],
            ['raza' => 'Shetland Sheepdog', 'pmin' => 6, 'pmax' => 14, 'comidamin' => 100, 'comidamed' => 175, 'comidamax' => 250, 'recomendedTime' => 30],
            ['raza' => 'Shar Pei', 'pmin' => 18, 'pmax' => 29, 'comidamin' => 350, 'comidamed' => 400, 'comidamax' => 500, 'recomendedTime' => 60],
            ['raza' => 'Jack Russell Terrier', 'pmin' => 5, 'pmax' => 8, 'comidamin' => 100, 'comidamed' => 150, 'comidamax' => 200, 'recomendedTime' => 30],
            ['raza' => 'Cocker Spaniel', 'pmin' => 11, 'pmax' => 15, 'comidamin' => 200, 'comidamed' => 250, 'comidamax' => 300, 'recomendedTime' => 30],
            ['raza' => 'English Springer Spaniel', 'pmin' => 18, 'pmax' => 23, 'comidamin' => 300, 'comidamed' => 350, 'comidamax' => 400, 'recomendedTime' => 30],
            ['raza' => 'West Highland White Terrier', 'pmin' => 6, 'pmax' => 10, 'comidamin' => 100, 'comidamed' => 150, 'comidamax' => 200, 'recomendedTime' => 30],
            ['raza' => 'Scottish Terrier', 'pmin' => 8, 'pmax' => 10, 'comidamin' => 140, 'comidamed' => 165, 'comidamax' => 180, 'recomendedTime' => 60],
        ];

        foreach ($razas as $raza) {
            DB::table('razas')->insert([
                'raza' => $raza,
            ]);
        }
    }
}
