<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VaccinePetCenterSeeder extends Seeder
{
    public function run(): void
    {
        $records = [];

        for ($i = 1; $i <= 20; $i++) {
            $petCenterId = rand(1, 20);
            $vaccineId = rand(1, 5);
            $price = rand(20, 100);

            $records[] = [
                'pet_center_id'         => $petCenterId,
                'vaccine_id'            => $vaccineId,
                'price'                 => $price,
                'created_at'            => now(),
                'updated_at'            => now(),
            ];
        }

        foreach ($records as $record) {
            DB::table('pet_center_vaccines')->insert([
                'record' => $record,
            ]);
        }
    }
}
