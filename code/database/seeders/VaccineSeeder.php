<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Vaccine;
use Illuminate\Support\Facades\DB;

class VaccineSeeder extends Seeder
{
    public function run(): void
    {
        $vaccines = [
            [
                'type_of_vaccine'       => 'Primera vacuna cachorros',
                'weeks_administration'  => 6,
                'description'           => 'Parvovirus y Moquillo',
                'company'               => 'Apex company',
                'is_annual'             => false,
                'is_mandatory'          => true,
                'created_at' => now(),
                'updated_at' => now(),
            ],


            [
                'type_of_vaccine'       => 'Polivalente canina 8s',
                'weeks_administration'  => 8,
                'description'           => 'Parvovirus, Moquillo, Hepatitis, Leptospirosis y parainfluenza',
                'company'               => 'Andromeda lab corp.',
                'is_annual'             => false,
                'is_mandatory'          => true,
                'created_at' => now(),
                'updated_at' => now(),
            ],


            [
                'type_of_vaccine'       => 'Polivalente canina 12s',
                'weeks_administration'  => 12,
                'description'           => 'Parvovirus, Moquillo, Hepatitis, Leptospirosis y parainfluenza',
                'company'               => 'Gold Pharma',
                'is_annual'             => false,
                'is_mandatory'          => true,
                'created_at' => now(),
                'updated_at' => now(),
            ],


            [
                'type_of_vaccine'       => 'Antirábica',
                'weeks_administration'  => 16,
                'description'           => 'Rábia',
                'company'               => 'Nirvava Labs',
                'is_annual'             => false,
                'is_mandatory'          => true,
                'created_at' => now(),
                'updated_at' => now(),
            ],


            [
                'type_of_vaccine'       => 'Polivalente rábia',
                'weeks_administration'  => 52,
                'description'           => 'Rábia, parvovirus, Moquillo, Hepatitis, Leptospirosis y parainfluenza',
                'company'               => 'Porteus ltd',
                'is_annual'             => true,
                'is_mandatory'          => true,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ];

        foreach ($vaccines as $vaccine) {
            DB::table('vaccines')->insert([
                'vaccine' => $vaccine,
            ]);
        }
    }
}
