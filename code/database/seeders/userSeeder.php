<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->create([
            'username' => 'adrian',
            'nombre' => 'Adrian',
            'email' => 'adri@test.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
       User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        User::factory()->create([
            'username' => 'diego',
            'nombre' => 'Diego',
            'email' => 'diego@test.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        // $user = $this->command->ask('Cuantos usuarios quieres insertar? ', 50);
        // User::factory($user)->create();
    }
}
