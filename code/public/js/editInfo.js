const btn_edit = document.getElementById('btn_edit');
const btn_check = document.getElementById('btn_check');
const btn_cancel = document.getElementById('btn_cancelar');
const usernameSelect = document.getElementById('username');
const nombreSelect = document.getElementById('nombre');
const emailSelect = document.getElementById('email');
const titleInfoUsername = document.getElementById('titleInfoUsername');
const titleInfoNombre = document.getElementById('titleInfoNombre');
const titleInfoEmail = document.getElementById('titleInfoEmail');
const inputs = document.querySelectorAll('.login_form');
const info = document.querySelectorAll('.userInfo');






btn_edit.addEventListener("click", function(){
    btn_edit.style.display = 'none';
    titleInfoUsername.style.display = 'none';
    titleInfoNombre.style.display = 'none';
    titleInfoEmail.style.display = 'none';
    btn_check.style.display = '';
    btn_cancel.style.display = '';
    usernameSelect.style.display = '';
    nombreSelect.style.display = '';
    emailSelect.style.display = '';
})
btn_cancel.addEventListener("click", function(){
    btn_edit.style.display = '';
    titleInfoUsername.style.display = '';
    titleInfoNombre.style.display = '';
    titleInfoEmail.style.display = '';
    btn_check.style.display = 'none';
    btn_cancel.style.display = 'none';
    usernameSelect.style.display = 'none';
    nombreSelect.style.display = 'none';
    emailSelect.style.display = 'none';
})
btn_check.addEventListener("click", function(){
    array = [];
    const username = usernameSelect.value
    const nombre = nombreSelect.value
    const email = emailSelect.value
    const user = {username, nombre, email};

    fetch("/editar-perfil", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            "X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').content // Agrega esta línea para incluir el token CSRF
        },
        body: JSON.stringify({username, nombre, email})
    });
    window.location.reload();

})