const passInputRegister = document.getElementById('password');

const upperText = document.getElementById('mayuscula');
const minusText = document.getElementById('minuscula');
const digitText = document.getElementById('numero');
const lenghtText = document.getElementById('lenght');
const equalsText = document.getElementById('passEquals');
const patternUpper = /(?=.*[A-Z])/
const patternMinus = /(?=.*[a-z])/
const patternDigit = /(?=.*[0-9])/

function comprobacionPassword() {
    const passRegisterValue = passInputRegister.value;


    if (passRegisterValue.length === 0) {
        upperText.setAttribute("hidden", false);
        minusText.setAttribute("hidden", false);
        digitText.setAttribute("hidden", false);
        lenghtText.setAttribute("hidden", false);
    } else if (passRegisterValue.length != 0) {
        upperText.removeAttribute("hidden");
        minusText.removeAttribute("hidden");
        digitText.removeAttribute("hidden");
        lenghtText.removeAttribute("hidden");
    }

    // CHECK LENGTH
    if (passRegisterValue.length > 7) {
        lenghtText.classList.remove("errorpass");
        lenghtText.classList.add("validated");
    } else if (passRegisterValue.length <= 7) {
        lenghtText.classList.remove("validated");
        lenghtText.classList.add("errorpass");
    }

    // CHECK MAYUS
    if (passRegisterValue.match(patternUpper)) {
        upperText.classList.remove("errorpass");
        upperText.classList.add("validated");
    } else if (!passRegisterValue.match(patternUpper)) {
        upperText.classList.remove("validated");
        upperText.classList.add("errorpass");
    }

    // CHECK MINUS
    if (passRegisterValue.match(patternMinus)) {
        minusText.classList.remove("errorpass");
        minusText.classList.add("validated");
    } else if (!passRegisterValue.match(patternMinus)) {
        minusText.classList.remove("validated");
        minusText.classList.add("errorpass");
    }

    // CHECK DIGIT
    if (passRegisterValue.match(patternDigit)) {
        digitText.classList.remove("errorpass");
        digitText.classList.add("validated");
    } else if (!passRegisterValue.match(patternDigit)) {
        digitText.classList.remove("validated");
        digitText.classList.add("errorpass");
    }



}
if (passInputRegister !== null) {
    passInputRegister.addEventListener('input', function (e) {
        comprobacionPassword();
    });

}
