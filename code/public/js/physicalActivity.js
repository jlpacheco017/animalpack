let addPhysicalActivityBtn = document.querySelectorAll(".addPhysicalActivityBtn")

const activatePhysicalActivity = async (id) => {
    try {
        const response = await fetch(`/petDetails/${id}/activatePhysicalActivity`);
        if (response.ok) {
            const data = await response.json();
            return data;
        } else {
            return null;
        }
    } catch (error) {
        return null;
    }
}

function changeToPhysicalActivityView() {
    console.log("hola");
    vistaVacunasContainer.classList.add("disiplayNone");
    vistaAlimentacioContainer.classList.add("disiplayNone");
    vistaFormaFisicaContainer.classList.remove("disiplayNone");

    vistaFormaFisicaSelector.parentNode.classList = "selector_ofrecemos select_feedback";
    vistaVacunasSelector.parentNode.classList = "selector_ofrecemos";
    vistaAlimentacionSelector.parentNode.classList = "selector_ofrecemos";
}


addPhysicalActivityBtn.forEach((element) => {
    element.addEventListener("click", function (e) {
        activatePhysicalActivity(parseInt(element.id)).then((res) => {
            if (res !== true) {
                window.location.reload();
                window.onload = changeToPhysicalActivityView;
                return;
            }

            return;
        });
    });
})



