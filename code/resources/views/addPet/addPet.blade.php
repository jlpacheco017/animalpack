@extends('layouts.master')
@section('title', 'AnimalPack - Nuevo Animal')
@section('content')
    <div class="add-pet-container">
        <form action="" method="post" id="form-add-animal" enctype="multipart/form-data">
            @csrf
            <h2>Añade tu mascota</h2>

            <div class="login_inputs">
                <label for="nombreMascota">Nombre Mascota</label>
                <input class="login_form" id="nombreMascota" type="text" name="name" placeholder="Nombre" />
            </div>
            <div class="login_inputs">
                <label for="nacimientoMascota">Fecha de nacimiento:</label>
                <input class="login_form" id="nacimientoMascotaInput" type="date" value="" name="age" />
            </div>

            <div class="login_inputs">
                <label for="razaMascota">Raza:</label>
                <select name="breed" id="razaMascota">
                    <option class="login_form" value="" disabled selected>Selecciona una raza...</option>
                    @foreach ($razas as $key => $raza)
                        <option value="{{ $raza->raza }}">{{ $raza->raza }}</option>
                    @endforeach
                </select>
            </div>

            <div class="login_inputs">
                <label for="pesoMascota">Peso:</label>
                <input class="login_form" name="weight" id="pesoMascota"type="text" onkeypress="validate(event)"
                    placeholder="20 Kg" />
            </div>
            <div class="login_inputs">
                <label for="pesoMascota">Peso:</label>
                <input class="login_form" type="file" name="fotoMascota" class="img-select"
                    accept="image/png, image/gif, image/jpeg, image/jpg" />
            </div>
            @if ($errors->has('name'))
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $errors->first('name') }}
                </div>
            @endif
            @if ($errors->has('breed'))
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $errors->first('breed') }}
                </div>
            @endif
            @if ($errors->has('weight'))
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $errors->first('weight') }}
                </div>
            @endif
            @if ($errors->has('age'))
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $errors->first('age') }}
                </div>
            @endif
            @if ($errors->has('fotoMascota'))
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $errors->first('fotoMascota') }}
                </div>
            @endif

            <button type="submit" class="btn btn-primary btn_avance2" value="Crear"><b>Crear</b></button>
        </form>
    </div>

@endsection
