@extends('layouts.master')
@section('title', 'AnimalPack - Login')
@section('content')
<div class="login">
        <div class="login_superior">
            <img class="foto_login" src="{{ asset('img/login_image.png') }}" srcset="">
        </div>
        <form method="post">
            @csrf
            <h1 class="titulo_login">INICIAR SESION</h1>
            
            <div class="login_inputs">
                <i id="login_icon" class="fa-solid fa-user"></i>
                <input class="login_form" type="text" id="email" name="email" placeholder="Usuario" required>

            </div>
            <div class="login_inputs">
                <i id="login_icon" class="fa-solid fa-lock"></i>
                <input class="login_form" type="password" id="password" name="password" placeholder="Contraseña"
                    required>
            </div>
            @error('login')
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $message }}
                </div>
            @enderror
            @if (session('succes'))
                <div class="succes-message">
                    <i class='bx bx-error'></i> {{ session('succes') }}
                </div>
            @endif
            <button type="submit" class="btn btn-primary btn_avance2">Login</button>
            <a href="{{ route('auth.showRegister') }}" id="btn_volver" class="btn_volver2"><b>REGISTRARSE</b></a>
    </form>
</div>
</div>
@endsection
