@extends('layouts.master')
@section('title', 'AnimalPack - Register')
@section('content')
    <div class="login">
        <div class="login_superior">
            <img class="foto_login" src="{{ asset('img/login_image.png') }}" srcset="">
        </div>
        <form class="register" method="POST">
            <h1><span style="color: #ED33B9;">REGISTRARSE</span>
            </h1>
            @csrf
            
            <div class="login_inputs">
                <i id="login_icon" class="fa-solid fa-user"></i>
                <input class="login_form" type="text" id="username" name="username" placeholder="Nombre de Usuario"
                    required>
            </div>
            <div class="login_inputs">
                <i id="login_icon" class="fa-solid fa-user"></i>
                <input class="login_form" type="text" id="name" name="name" placeholder="Nombre Completo"
                    required>
            </div>
            <div class="login_inputs">
                <i class="fa-solid fa-envelope" id="login_icon"></i>
                <input class="login_form" type="text" id="email" name="email" placeholder="Email" required>
            </div>
            <div class="login_inputs">
                <i class="fa-solid fa-lock" id="login_icon"></i>
                <input class="login_form" type="password" id="password" name="password" placeholder="Contraseña" required>
            </div>


            <div class="password_feedback">
                    <p hidden id="lenght" class="text-p-small errorpass">● Debe contener almenos 8 caracteres</p>
                    <p hidden id="mayuscula" class="text-p-small errorpass">● Una letra en mayúscula</p>
                    <p hidden id="minuscula" class="text-p-small errorpass">● Una letra en minúscula</p>
                    <p hidden id="numero" class="text-p-small errorpass"> ● Un numero</p>

    
            </div>

            @if ($errors->has('username'))
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $errors->first('username') }}
                </div>
            @endif
            @if ($errors->has('email'))
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $errors->first('email') }}
                </div>
            @endif
            @if ($errors->has('password'))
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $errors->first('password') }}
                </div>
            @endif
            @if ($errors->has('name'))
            <div class="error-message">
                <i class='bx bx-error'></i> {{ $errors->first('name') }}
            </div>
        @endif
            <button type="submit" id="btn_avance" class="btn btn-primary btn_avance2">Registrarse</button>
            <a href="{{ route('home.index') }}" id="btn_volver" class="btn_volver2">Volver</a>


        </form>
    </div>
@endsection
@section('js')
    <script src="/js/header.js"></script>
    <script src="/js/passwordCheck.js"></script>
@endsection
