@if ($physicalActivity->first() != null)
    <table class="formaFisicaTable">
        <tr>
            <td>
                Fecha inicio
            </td>
            <td>
                Fecha final
            </td>
            <td>
                Tiempo
            </td>
        </tr>
        @foreach ($physicalActivity as $pa)
            <tr class="userInfo">
                <td>
                    {{ date_create($pa->started_at)->format('d-m-Y H:i:s') }}
                </td>
                <td>
                    {{ date_create($pa->finished_at)->format('d-m-Y H:i:s') }}
                </td>
                <td>
                    {{ date_create($pa->started_at)->diff(date_create($pa->finished_at))->format('%H:%I:%S') }}
                </td>
            </tr>
        @endforeach
    </table>
    <div class="addPhysicalActivityBtnContainer">
        <div class="physicalActivityfullWidth">
            <button id="{{ $pet->id }}"
                class="addPhysicalActivityBtn btn btn-primary btn_avance2">Activar/Desactivar</button>
        </div>
        <div class="physicalActivityfullWidth">
            <p class="physicalActivityNota">Nota: Pulsa una vez para indicar el inicio de la forma física y pulsa otra
                vez para indicar el final.</p>
        </div>
    </div>
    <div class="totalTimeContainer">
        <p class="physicalActivitiesRecomendedTime">Te recomendadmos al rededor de <span
                class="userInfo">{{ $recomendedTime }}</span> minutos de actividad física al dia para la raza de tu
            perro.</p>
        <p class="physicalActivitiesTotalTime">Total: <span class="userInfo">{{ $time }}</span></p>
    </div>
@else
    <p class="physicalError">No hay actividad física registrada</p>
    <div class="physicalActivityfullWidth">
        <button id="{{ $pet->id }}"
            class="addPhysicalActivityBtn btn btn-primary btn_avance2">Activar/Desactivar</button>
    </div>
    <div class="physicalActivityfullWidth">
        <p class="physicalActivityNota">Nota: Pulsa una vez para indicar el inicio de la forma física y pulsa otra
            vez para indicar el final.</p>
    </div>
@endif
