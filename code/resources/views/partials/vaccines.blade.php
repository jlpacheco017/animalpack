<div class="info-razas-container">
    <script src="https://cdn.lordicon.com/bhenfmcm.js"></script>
    <h2 class="titulo-funcion"> Siguientes vacunas </h2>

    <div style="overflow-x:auto;">
        <table class="formaFisicaTable">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Fecha</th>
                    <th>Centro</th>
                    <th>Legal</th>
                    <th>Aplicada</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($nextVaccine as $vacine)
                    <tr>
                        <td>{{ $vacine->type_of_vaccine }}</td>
                        <td>{{ $vacine->scheduled_date }}</td>
                        <td>
                            @if (empty($vacine->center_name))
                                <a
                                    href="{{ route('petDetail.centerDetails', ['id_vaccine' => $vacine->id, 'id' => $pet->id]) }}">Buscar centro</a>
                            @else
                                {{ $vacine->center_name }}
                            @endif
                        </td>
                        <td>{{ $vacine->is_mandatory == 1 ? 'Obligatoria' : 'Opcional' }}</td>
                        <td>No</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <h2 class="titulo-funcion"> Histórico vacunas </h2>

    <div style="overflow-x:auto;">
        <table class="formaFisicaTable">
            <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Fecha</th>
                    <th>Centro</th>
                    <th>Legal</th>
                    <th>Aplicada</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($vaccineHistory as $dato)
                    <tr>
                        <td>{{ $dato->pet_center_vaccine->vaccine->type_of_vaccine }}</td>
                        <td>{{ $dato->date_applied }}</td>
                        <td>{{ $dato->pet_center_vaccine->pet_center->name }}</td>
                        <td>{{ $dato->mandatory == 1 ? 'Obligatoria' : 'Opcional' }}</td>
                        <td>{{ $dato->applied == 1 ? 'Si' : 'No' }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</div>
