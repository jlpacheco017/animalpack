@extends('layouts.master')
@section('title', 'AnimalPack - Detalles')
@section('content')
    @if ($pet)
        <div id="container-detalles-animal">
            <div id="{{ $pet->id }}" class="detalles-animal">
                <div class="pet-details-right-div">
                    <a class="backArrow" href="{{ route('petDetail.showPetDetail', ['id' => $pet->id])}}">&#8592; Volver</a>

                    <div class="pet-details-name-container">
                        <h1 class="dog_name" id="nameInfotitle">Buscar centro</h1>
                    </div>

                    <div class="pet-details-pet-center" >
                        <div class="vaccine-table" style="overflow-x:auto;">
                            <table class="formaFisicaTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Centro Veterinario</th>
                                        <th>Dirección</th>
                                        <th>CP</th>
                                        <th>Ciudad</th>
                                        <th>Teléfono</th>
                                        <th>Precio</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($centers as $center)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $center->petCenter->name }}</td>
                                            <td>{{ $center->petCenter->address }}</td>
                                            <td>{{ $center->petCenter->postal_code }}</td>
                                            <td>{{ $center->petCenter->city }}</td>
                                            <td>{{ $center->petCenter->phone }}</td>
                                            <td>{{ $center->price }}€</td>
                                            <td>
                                                <form action="{{ route('petDetail.SelectionCenterDetails') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="idCenterVaccine" value="{{ $center->id }}">
                                                    <input type="hidden" name="idPet" value="{{ $pet->id }}">
                                                    <input type="hidden" name="price" value="{{ $center->price }}">
                                                    <button type="submit" class="btn btn-primary">Seleccionar</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif
@endsection
@section('js')
    <script src="/js/editPet.js"></script>
    <script src="/js/changePetDetailsView.js"></script>
@endsection
