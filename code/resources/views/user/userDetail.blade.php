@extends('layouts.master')
@section('title', 'AnimalPack - HOME')
@section('content')
    <div class="userDetail_container">

        <div class="user-section">
            <h2>INFORMACION DEL USUARIO</h2>
            <div class="userNameContainer">
                @if (session('validation'))
                    <div class="error-message">{{ session('validation') }}</div>
                @endif
                @if (session('succes'))
                    <div class="succes-message">{{ session('succes') }}</div>
                @endif
                <p class="titleInfo">Nombre de usuario</p>
                <p class="userInfo" id="titleInfoUsername">{{ $user->username }}</p>
                <input style="display: none;" class="login_form" type="text" id="username" name="username"
                    value="{{ $user->username }}">
            </div>
            
            <div class="userNameContainer">
                <p class="titleInfo">Nombre</p>
                <p class="userInfo" id="titleInfoNombre">{{ $user->nombre }}</p>
                <input style="display: none;" class="login_form" type="text" id="nombre" name="username"
                    value="{{ $user->nombre }}">
            </div>
            <div class="userNameContainer">
                <p class="titleInfo">Email</p>
                <p class="userInfo" id="titleInfoEmail">{{ $user->email }}</p>
                <input style="display: none;" class="login_form" type="text" id="email" name="username"
                    value="{{ $user->email }}">
            </div>
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <div class="edit-profile">
                <button id="btn_edit" class="btn btn-primary btn_avance2">Editar <i class="fa-solid fa-pen" style="color: #000000;"></i></button>
                <button id="btn_check" style="display: none;"><i class="fa-regular fa-circle-check"
                        style="color: #57ae37; font-size: 2rem;"></i></button>
                <button id="btn_cancelar" style="display: none;"><i class="fa-sharp fa-regular fa-circle-xmark"
                        style="color: #a84038; font-size: 2rem;"></i></button>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="/js/editInfo.js"></script>
@endsection
