<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Database\Seeders\userSeeder;
use Illuminate\Support\Facades\Log;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function test_entrar_con_un_usuario_por_su_email()
    {
        // CREAR UN USUARIO.
        User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);
        // ENTRAMOS CON EL USUARIO CREADO.
        $response = $this->post(Route('auth.doLogin'), [
            'email' => 'jlpacheco@gmail.com',
            'password' => '1234',
        ]);
        // COMPROBAR QUE HA HECHO LA REDIRECIÓN A LA 'LANDING'.
        $response->assertRedirect(Route('home.home'));
        $user = User::find(1);
        // COMPROBAR QUE ESTAMOS AUTENTICADOS COMO EL USUARIO CREADO.
        $this->assertAuthenticatedAs($user);
    }

    public function test_entrar_con_un_usuario_por_su_nombre()
    {
        // CREAR UN USUARIO.
        User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);

        // ENTRAMOS CON EL USUARIO CREADO.
        $response = $this->post(Route('auth.doLogin'), [
            'email' => 'jlpacheco',
            'password' => '1234',
        ]);

        $response->assertRedirect(Route('home.home'));
        $user = User::find(1);
        // COMPROBAR QUE ESTAMOS AUTENTICADOS COMO EL USUARIO CREADO.
        $this->assertAuthenticatedAs($user);
    }

    public function test_entrar_con_un_usuario_que_no_existe()
    {
        // ENTRAMOS CON UN USUARIO QUE NO HEMOS CREADO.
        $response = $this->post(Route('auth.doLogin'), [
            'email' => 'jlpacheco',
            'password' => '1234',
        ]);

        // COMPROBAR QUE HA HECHO LA REDIRECIÓN A LA 'LANDING'.
        $response->assertSessionHasErrors('login');
    }
    public function test_entrar_con_un_usuario_con_una_contraseña_incorrecta()
    {
        // CREAR UN USUARIO.
        User::factory()->create([
            'username' => 'jlpacheco',
            'nombre' => 'Pacheco',
            'email' => 'jlpacheco@gmail.com',
            'password' => Hash::make('1234'),
            'subscripcion' => 'Premium'
        ]);

        // ENTRAMOS CON UN USUARIO QUE NO HEMOS CREADO.
        $response = $this->post(Route('auth.doLogin'), [
            'email' => 'jlpacheco',
            'password' => '12345',
        ]);

        // COMPROBAR QUE HA HECHO LA REDIRECIÓN A LA 'LANDING'.
        $response->assertSessionHasErrors('login');
    }

}
